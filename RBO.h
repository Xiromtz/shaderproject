#pragma once
#include <glad/glad.h>

class RBO
{
public:
    GLuint ID;

    RBO();
    ~RBO();

    void bindToFBO(GLuint fbo);
    void bind();
    void unbind();
};

