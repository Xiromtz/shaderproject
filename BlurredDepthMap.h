#pragma once
#include "Texture.h"
#include "VBO.h"
#include "VAO.h"
#include "GLShader.h"

class BlurredDepthMap
{
private:
    
    size_t width, height;

    GLuint FBO;
    
    VBO* vbo = nullptr;
    VAO* vao = nullptr;
    GLShader* shader;

    float vertices[12]
    {
        -1.0f,  1.0f,
        -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f,  1.0f,
        1.0f, -1.0f,
        1.0f,  1.0f
    };
public:
    GLuint blurredDepthmapID;
    GLuint horizontalBlurredDepthmapID;

    BlurredDepthMap(size_t width, size_t height);
    ~BlurredDepthMap();

    void draw(Texture* depthMap);
};

