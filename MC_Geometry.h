#pragma once
#include "Triangle.h"

class MC_Geometry : public GLObject
{
private:
    size_t vertexCount;
    glm::vec3* vertices;

    std::vector<Triangle*> triangles;

    Texture* tex2;
    Texture* tex3;

    std::vector<glm::vec3> vertexPositions;

    void setVertexPositions(glm::vec3* vertices, size_t size);
public:
    MC_Geometry(glm::vec3* vertices, size_t size);
    ~MC_Geometry();

    void draw(const GLShader &shader) const;
    void setTriangles();
    std::vector<Triangle*> getTriangles() const override;
    Triangle* getClosestIntersectingTriangle(const glm::vec3& a, const glm::vec3& d);
};

