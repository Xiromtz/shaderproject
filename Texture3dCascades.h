#pragma once

#include "Texture.h"
#include "FBO.h"
#include "GLShader.h"
#include "VertexAttributes.h"
#include "VAO.h"
#include "VBO.h"

class Texture3dCascades
{
private:
    const int WIDTH = 96, HEIGHT = 96, DEPTH = 256;
    float vertices[12] = {
     -1.0f,  1.0f ,
     -1.0f, -1.0f ,
     1.0f, -1.0f,
     -1.0f,  1.0f ,
     1.0f, -1.0f ,
     1.0f,  1.0f 
    };

    Texture texture;

    FBO fbo;
    GLuint vboID = 0;
    GLuint vaoID = 0;
    VBO* vbo = nullptr;
    VAO* vao = nullptr;
    GLShader shader;
public:
    Texture3dCascades();
    ~Texture3dCascades();

    void draw();
    void bind() const;
    void unbind() const;
    Texture get3dTexture();
};

