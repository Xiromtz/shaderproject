#pragma once
#include <glm/glm.hpp>
#include <glad/glad.h>

class Particle
{
public:
    glm::vec3 position, velocity;
    glm::vec4 color;
    float size, angle, weight;
    float life;

    Particle() : position(0.0f), velocity(0.0f), color(1.0f), life(0.0f) {}
    ~Particle(){}
};

