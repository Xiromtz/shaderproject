#pragma once
#include <glm/detail/type_vec3.hpp>
#include <vector>
#include <algorithm>
#include "Triangle.h"

struct node
{
    glm::vec3 point;
    int splitAxis;
    struct node *left, *right;
};

class KD_Tree
{
public:
    
private:
    inline double dist(node* a, node* b, int dim);
    inline void swap(node* x, node* y);
    node* find_median(node* start, node* end, int idx);
    node* make_tree(node* t, int len, int i, int dim);

    std::vector<node> unorderedTree;
public:
    node * root = nullptr;
    node* getClosestNodeAlongRay(node* current, const glm::vec3& a, const glm::vec3& d, float tMax, node* closest);

    KD_Tree(const std::vector<glm::vec3>& points);
    ~KD_Tree();
};

