#pragma once
#include "Triangle.h"

class Cube : public GLObject
{
private:
    const char* texturePath = "Texture/cube1.png";
    const char* normalTexturePath = "Texture/cube1_normal.png";

    Texture* normalTex;
    std::vector<Triangle*> triangles;

    float* vertices = new float[192]{
        // back face
        // position				// normal			// texture
        -0.5f, -0.5f, -0.5f,	0.0f, 0.0f, -1.0f,	0.0f, 0.0f,
        0.5f, -0.5f, -0.5f,		0.0f, 0.0f, -1.0f,	1.0f, 0.0f,
        0.5f,  0.5f, -0.5f,		0.0f, 0.0f, -1.0f,  1.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,	0.0f, 0.0f, -1.0f,  0.0f, 1.0f,

        //front face
        -0.5f, -0.5f,  0.5f,	0.0f, 0.0f, 1.0f,  0.0f, 0.0f,
        0.5f, -0.5f,  0.5f,		0.0f, 0.0f, 1.0f,  1.0f, 0.0f,
        0.5f,  0.5f,  0.5f,		0.0f, 0.0f, 1.0f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,	0.0f, 0.0f, 1.0f,  0.0f, 1.0f,

        //left face
        -0.5f,  0.5f,  0.5f,	-1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,	-1.0f, 0.0f, 0.0f,  1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,	-1.0f, 0.0f, 0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,	-1.0f, 0.0f, 0.0f,  0.0f, 0.0f,

        //right face
        0.5f,  0.5f,  0.5f,		1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
        0.5f,  0.5f, -0.5f,		1.0f, 0.0f, 0.0f,  1.0f, 1.0f,
        0.5f, -0.5f, -0.5f,		1.0f, 0.0f, 0.0f,  0.0f, 1.0f,
        0.5f, -0.5f,  0.5f,		1.0f, 0.0f, 0.0f,  0.0f, 0.0f,

        //bottom face
        -0.5f, -0.5f, -0.5f,	0.0f, -1.0f, 0.0f,  0.0f, 1.0f,
        0.5f, -0.5f, -0.5f,		0.0f, -1.0f, 0.0f,  1.0f, 1.0f,
        0.5f, -0.5f,  0.5f,		0.0f, -1.0f, 0.0f,  1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,	0.0f, -1.0f, 0.0f,  0.0f, 0.0f,

        //top face
        -0.5f,  0.5f, -0.5f,	0.0f, 1.0f, 0.0f,  0.0f, 1.0f,
        0.5f,  0.5f, -0.5f,		0.0f, 1.0f, 0.0f,  1.0f, 1.0f,
        0.5f,  0.5f,  0.5f,		0.0f, 1.0f, 0.0f,  1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,	0.0f, 1.0f, 0.0f,  0.0f, 0.0f,
    };

    unsigned int indices[36] =
    {
        0,1,2,2,3,0,
        4,5,6,6,7,4,
        8,9,10,10,11,8,
        12,13,14,14,15,12,
        16,17,18,18,19,16,
        20,21,22,22,23,20
    };

    void setup();
    void addTangents();
public:
    explicit Cube();
    explicit Cube(const char* texturePath, const char* normalTexturePath);
    ~Cube();

    void draw(const GLShader &shader) const override;
    void setTriangles();
    Triangle* getIntersectingTriangle(const glm::vec3& origin, const glm::vec3& rayDirection, float tMax);
    std::vector<Triangle*> getTriangles() const override;
};

