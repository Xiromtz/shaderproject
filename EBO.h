#pragma once
#include <glad/glad.h>
#include "VBO.h"
#include "VAO.h"

class EBO
{
public:
	GLuint ID;
	EBO(const VertexAttributes &attrs, const VAO& vao, const VBO& vbo);
	~EBO();

	void bind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ID);
	}

	void unbind() const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
};

