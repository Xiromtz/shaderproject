#pragma once

#include "stb_image.h"
#include <glad/glad.h>
#include <iostream>

class Texture
{
private:
	int width, height, depth, nrChannels;
	unsigned char* data = nullptr;
	GLenum textureNum;

    bool is3dTexture = false;
	
	void loadImage(const char* path);
	void generateTexture(GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum format, GLenum type);
    void generateTexture(GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum internalFormat, GLenum format, GLenum type);
    void generate3dTexture(GLint s_wrap, GLint t_wrap, GLint r_wrap, GLint minFilter, GLint magFilter, GLenum internalFormat, GLenum format, GLenum type);
public:

	GLuint ID;
	Texture(const char* path, GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum textureNum, GLenum format, GLenum type);
	Texture(int width, int height, GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum textureNum, GLenum format, GLenum type);
    Texture(int width, int height, GLint s_wrap, GLint t_wrap, GLint minFilter, GLint magFilter, GLenum textureNum, GLenum internalFormat, GLenum format, GLenum type);
    Texture(int width, int height, int depth, GLint s_wrap, GLint t_wrap, GLint r_wrap, GLint minFilter, GLint magFilter, GLenum textureNum, GLenum internalFormat, GLenum format, GLenum type);
    ~Texture();

    
	
	inline void bind() const
	{
        if (is3dTexture)
        {
            bind3d();
            return;
        }

		glActiveTexture(textureNum);
		glBindTexture(GL_TEXTURE_2D, ID);
	}

	inline void unbind() const
	{
        if (is3dTexture)
        {
            unbind3d();
            return;
        }

		glBindTexture(GL_TEXTURE_2D, 0);
	}
    
private:
    void bind3d() const
    {
        glActiveTexture(textureNum);
        glBindTexture(GL_TEXTURE_3D, ID);
    }

    void unbind3d() const
    {
        glBindTexture(GL_TEXTURE_3D, 0);
    }
};

