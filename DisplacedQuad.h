#pragma once
#include "GLObject.h"

class DisplacedQuad : public GLObject
{
private:
    Texture* normalMap;
    Texture* depthMap;

    std::vector<GLfloat> vertices;

    void createTextures();
    void createVertices();
    void createBuffers();
public:
    DisplacedQuad();
    ~DisplacedQuad();

    void draw(const GLShader& shader) const override;
    std::vector<Triangle*> getTriangles() const override { return {}; }
};

