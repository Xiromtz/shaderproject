#pragma once
#include <vector>
#include <glad/glad.h>

class VertexAttributes
{
public:
	GLsizei stride;
	std::vector<int> offsets;
	std::vector<GLint> sizes;

	float* vertices;
	unsigned int vertexArraySize;
	unsigned int* indices;
	unsigned int indexArraySize;

	VertexAttributes(float* vertices, unsigned int vertexArraySize, unsigned int* indices, unsigned int indexArraySize)
		: vertices(vertices), vertexArraySize(vertexArraySize), indices(indices), indexArraySize(indexArraySize){}
	~VertexAttributes();

    void set_2d_coordinate_attributes();
	void set_coordinate_attributes();
	void set_coordinate_color_attributes();
	void set_coordinate_color_texture_attributes();
	void set_coordinate_texture_attributes();
	void set_coordinate_normal_texture_tangent_attributes();
    void set_coordinate_normal_texture_tangent_bitangent_attributes();

	inline void setStride(GLsizei stride)
	{
		this->stride = stride;
	}
	inline void add(int offset, GLint size)
	{
		offsets.push_back(offset);
		sizes.push_back(size);
	}

	inline int size() const
	{
		return offsets.size();
	}
};

