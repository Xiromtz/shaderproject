﻿#version 330 core
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 velocity;
layout(location = 2) in float lifetime;
layout(location = 3) in uint seed;
layout(location = 4) in int type;

out particle
{
	vec3 position;
	vec3 velocity;
	flat uint seed;
	flat float lifetime;
	flat int type;
} vs_out;

void main()
{
	vs_out.position = position;
	vs_out.seed = seed;
	vs_out.lifetime = lifetime;
	vs_out.type = type;
	vs_out.velocity = velocity;
}