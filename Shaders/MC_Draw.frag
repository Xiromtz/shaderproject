﻿#version 330 core
out vec4 FragColor;

in vec3 pos_fs_in;
in vec2 texCoord_fs_in;
in vec3 normal_fs_in;

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform sampler2D texture3;

uniform vec3 lightPos;
uniform vec3 viewPos;

void main()
{
	vec3 normal = normal_fs_in;

	vec3 blend_weights = abs(normal) - 0.2;
	blend_weights *= 7;
	blend_weights.x = max(pow(blend_weights.x, 3.0), 0.0);
	blend_weights.y = max(pow(blend_weights.y, 3.0), 0.0);
	blend_weights.z = max(pow(blend_weights.z, 3.0), 0.0);
	blend_weights /= (blend_weights.x + blend_weights.y + blend_weights.z);

	vec3 color1 = texture(texture1, texCoord_fs_in).rgb;
	vec3 color2 = texture(texture2, texCoord_fs_in).rgb;
	vec3 color3 = texture(texture3, texCoord_fs_in).rgb;

	vec3 color = color1 * blend_weights.x + color2 * blend_weights.y + color3 * blend_weights.z;
	vec3 ambient = 0.05 * color;

	vec3 lightDir = normalize(lightPos - pos_fs_in);
	float diff = max(dot(lightDir, normal), 0.0);
	vec3 diffuse = diff * color;

	// Specular
	vec3 viewDir = normalize(viewPos - pos_fs_in);
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = 0.0;
	
	vec3 halfwayDir = normalize(lightDir + viewDir);
	spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);

	vec3 specular = vec3(0.3) * spec;

	color = ambient + diffuse + specular;
	FragColor = vec4(color, 1.0);
}