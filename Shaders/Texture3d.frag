﻿#version 430
layout(location = 0) out float density;

in GS_OUT
{
	vec3 ws;
} fs_in;

vec2 rotate(vec2 v, float a)
{
	float s = sin(a);
	float c = cos(a);
	mat2 m = mat2(c, -s, s, c);
	return m * v;
}

float AddPillar(vec3 ws, vec2 pos, float weight)
{
	float angle = ws.y * 3.1415926535897932384626433832795;
	pos = rotate(pos, angle);
	return weight * (1.0 / length(ws.xz - pos) - 1.0);
}

float AddPillars(vec3 ws)
{
	float f = 0.0;

	f += AddPillar(ws, vec2(0.0, 0.5), 0.5);
	f += AddPillar(ws, vec2(-0.4, -0.25), 0.5);
	f += AddPillar(ws, vec2(0.4, -0.25), 0.5);
	f += AddPillar(ws, vec2(0.0, 0.0), -1.0);

	return f;
}


float AddHelix(vec3 ws)
{
	float angle = ws.y * 3.1415926535897932384626433832795;
	vec2 vec = vec2(cos(angle), sin(angle));
	return 3 * dot(vec, ws.xz);
}

void main()
{
	density = 0.0;
	density += AddPillars(fs_in.ws);
	density -= 10 * pow (length(fs_in.ws.xz), 3);
	density += AddHelix(fs_in.ws);
	density += 0.5 * cos(fs_in.ws.y * 3.1415926535897932384626433832795);
}