﻿#version 330 core
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

in Particle
{
	flat int type;
	vec3 position;
} gs_in[];

out ParticleQuad
{
    flat int type;
    vec2 uv;
} gs_out;

uniform mat4 view;
uniform mat4 projection;

void main()
{
	if (gs_in[0].type == 1)
	{
		gl_Position = projection * view * (vec4(gs_in[0].position.x - 0.005,  gs_in[0].position.y - 0.02, gs_in[0].position.z, 1.0f));
		gs_out.uv = vec2(0, 0);
		gs_out.type = gs_in[0].type;
		EmitVertex();

		gl_Position = projection * view * (vec4(gs_in[0].position.x + 0.005, gs_in[0].position.y - 0.02, gs_in[0].position.z, 1.0f));
		gs_out.uv = vec2(0, 1);
		gs_out.type = gs_in[0].type;
		EmitVertex();

		gl_Position = projection * view * (vec4(gs_in[0].position.x - 0.005,  gs_in[0].position.y + 0.02, gs_in[0].position.z, 1.0f));
		gs_out.uv = vec2(1, 0);
		gs_out.type = gs_in[0].type;
		EmitVertex();

		gl_Position = projection * view * (vec4(gs_in[0].position.x + 0.005, gs_in[0].position.y + 0.02, gs_in[0].position.z, 1.0f));
		gs_out.uv = vec2(1, 1);
		gs_out.type = gs_in[0].type;
		EmitVertex();
		EndPrimitive();
	}
	else if (gs_in[0].type == 3)
	{
		gl_Position = projection * view * (vec4(gs_in[0].position.x - 0.005,  gs_in[0].position.y - 0.005, gs_in[0].position.z, 1.0f));
		gs_out.uv = vec2(0, 0);
		gs_out.type = gs_in[0].type;
		EmitVertex();

		gl_Position = projection * view * (vec4(gs_in[0].position.x + 0.005, gs_in[0].position.y - 0.005, gs_in[0].position.z, 1.0f));
		gs_out.uv = vec2(0, 1);
		gs_out.type = gs_in[0].type;
		EmitVertex();

		gl_Position = projection * view * (vec4(gs_in[0].position.x - 0.005,  gs_in[0].position.y + 0.005, gs_in[0].position.z, 1.0f));
		gs_out.uv = vec2(1, 0);
		gs_out.type = gs_in[0].type;
		EmitVertex();

		gl_Position = projection * view * (vec4(gs_in[0].position.x + 0.005, gs_in[0].position.y + 0.005, gs_in[0].position.z, 1.0f));
		gs_out.uv = vec2(1, 1);
		gs_out.type = gs_in[0].type;
		EmitVertex();
		EndPrimitive();
	}
}