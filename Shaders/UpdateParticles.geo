﻿#version 330 core
layout(points) in;
layout(points, max_vertices = 11) out;

in particle
{
	vec3 position;
	vec3 velocity;
	flat uint seed;
	flat float lifetime;
	flat int type;
	
} gs_in[];

struct ParticleOut
{
	vec3 position;
	vec3 velocity;
	uint seed;
	float lifetime;
	int type;
};
out ParticleOut gs_out;

uint currentSeed;
uniform float deltaTime;

uint xorshift32()
{
	currentSeed ^= currentSeed << 13;
	currentSeed ^= currentSeed >> 7;
	currentSeed ^= currentSeed << 17;
	return currentSeed;
}

vec3 newParticleRandomPosition(vec3 emitterPos)
{
	float xOffset = float(int(xorshift32() % 200u) - 100) / 100.0f;
	float zOffset = float(int(xorshift32() % 200u) - 100) / 100.0f;

	vec3 particlePos = vec3(emitterPos.x + xOffset, emitterPos.y, emitterPos.z + zOffset);
	return particlePos;
}

void createNewRainParticle(vec3 emitterPos)
{
	gs_out.position = newParticleRandomPosition(emitterPos);
	gs_out.velocity = vec3(0.0f, -1.0f, 0.0f);
	gs_out.seed = 0u;
	gs_out.lifetime = 0.0f;
	gs_out.type = 1;
}

void spawnNewRainParticles(vec3 emitterPos)
{
	for (int i = 0; i < 5; i++)
	{
		createNewRainParticle(emitterPos);
		EmitVertex();
		EndPrimitive();
	}
}

void createNewGasParticle(vec3 emitterPos)
{
	gs_out.position = emitterPos;

	float velocityX = float(int(xorshift32() % 400u) - 200);
	float velocityY = float(int(xorshift32() % 400u) - 200);
	float velocityZ = float(int(xorshift32() % 400u) - 200);
	gs_out.velocity = normalize(vec3(velocityX, velocityY, velocityZ));
	gs_out.velocity *= (float(int(xorshift32() % 200u) - 100) / 100.0f);

	gs_out.seed = 0u;
	gs_out.lifetime = 0.0f;
	gs_out.type = 3;
}

void spawnNewGasParticles(vec3 emitterPos)
{
	for (int i = 0; i < 10; i++)
	{
		createNewGasParticle(emitterPos);
		EmitVertex();
		EndPrimitive();
	}
}

void main()
{
	

	if (gs_in[0].type == 0)
	{
		currentSeed = gs_in[0].seed;
	
		spawnNewRainParticles(gs_in[0].position);

		gs_out.position = gs_in[0].position;
		gs_out.velocity = vec3(0,0,0);
		gs_out.seed = currentSeed;
		gs_out.lifetime = 0.0f;
		gs_out.type = 0;
		EmitVertex();
		EndPrimitive();
	}
	else if (gs_in[0].type == 1)
	{
		gs_out.position = gs_in[0].position + gs_in[0].velocity * deltaTime;
		gs_out.velocity = gs_in[0].velocity;
		gs_out.seed = 0u;
		gs_out.lifetime = gs_in[0].lifetime + deltaTime;
		gs_out.type = 1;

		if (gs_out.lifetime > 2.0f)
			return;

		EmitVertex();
		EndPrimitive();
	}
	else if (gs_in[0].type == 2)
	{
		currentSeed = gs_in[0].seed;
	
		spawnNewGasParticles(gs_in[0].position);

		gs_out.position = gs_in[0].position;
		gs_out.velocity = vec3(0,0,0);
		gs_out.seed = currentSeed;
		gs_out.lifetime = 0.0f;
		gs_out.type = 2;
		EmitVertex();
		EndPrimitive();
	}
	else if (gs_in[0].type == 3)
	{
		gs_out.position = gs_in[0].position + gs_in[0].velocity * deltaTime;
		gs_out.velocity = gs_in[0].velocity;
		gs_out.seed = 0u;
		gs_out.lifetime = gs_in[0].lifetime + deltaTime;
		gs_out.type = 3;

		if (gs_out.lifetime > 1.0f)
			return;

		EmitVertex();
		EndPrimitive();
	}
}