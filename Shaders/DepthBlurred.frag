﻿#version 430 core
layout(location = 0) out float depth;

uniform sampler2D depthMap;
uniform bool horizontal;

const float gaussFilter[7] = 
{ 
	1.f / 64.f,
	6.f / 64.f,
	15.f / 64.f,
	20.f / 64.f,
	15.f / 64.f,
	6.f / 64.f,
	1.f / 64.f
};

void main()
{
	ivec2 pixelPos = ivec2(gl_FragCoord.xy);
	ivec2 step = horizontal ? ivec2(1, 0) : ivec2(0, 1);

	ivec2 startPos = pixelPos - 3 * step;

	depth = 0.0f;
	ivec2 currentPos = startPos;
	for (int i = 0; i < 7; i++)
	{
		depth += texelFetch(depthMap, currentPos, 0).r * gaussFilter[i];
		currentPos += step;
	}
}  