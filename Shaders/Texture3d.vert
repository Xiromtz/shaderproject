﻿#version 430
layout(location = 0) in vec2 vPosition;

out VS_OUT
{
	int layer;
	vec3 ws;
} vs_out;

void main()
{
	vs_out.layer = gl_InstanceID;
	vs_out.ws = vec3(vPosition.x, 2.0 * (gl_InstanceID / 255.0 - 0.5), vPosition.y);
}