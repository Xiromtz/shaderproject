﻿#version 430
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;

out vec3 pos_cs_in;
out vec2 texCoord_cs_in;
out vec3 normal_cs_in;

void main()
{
	pos_cs_in = aPos;
	texCoord_cs_in = vec2(aPos.x * 1.5, aPos.y * 1.5);
	normal_cs_in = aNormal;
    //gl_Position = projection * view * model * vec4(aPos, 1.0);
}