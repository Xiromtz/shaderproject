﻿#version 430
layout(location = 0) in vec2 vPosition;

uniform sampler3D densityTex;

vec3 resolution = 2.0 / vec3(95.0, 255.0, 95.0);

out VS_OUT 
{
	vec3 cellCorners[8];
	float densityAtCorners[8];
	uint mc_case;
} vs_out;

vec3 ws_to_uvw(vec3 ws)
{
	vec3 scaled = ws * 0.5 + 0.5;
	return scaled.xzy;
}

void main()
{
	vec3 ws = vec3(vPosition.x, -1 + (resolution.y * (gl_InstanceID)), vPosition.y);
	
	vs_out.cellCorners[0] = ws + (resolution * vec3(0.0f, 0.0f, 0.0f));
	vs_out.cellCorners[1] = ws + (resolution * vec3(1.0f, 0.0f, 0.0f));
	vs_out.cellCorners[2] = ws + (resolution * vec3(1.0f, 0.0f, 1.0f));
	vs_out.cellCorners[3] = ws + (resolution * vec3(0.0f, 0.0f, 1.0f));
	vs_out.cellCorners[4] = ws + (resolution * vec3(0.0f, 1.0f, 0.0f));
	vs_out.cellCorners[5] = ws + (resolution * vec3(1.0f, 1.0f, 0.0f));
	vs_out.cellCorners[6] = ws + (resolution * vec3(1.0f, 1.0f, 1.0f));
	vs_out.cellCorners[7] = ws + (resolution * vec3(0.0f, 1.0f, 1.0f));

	for (int i = 0; i < 8; i++)
	{
		vec3 texCoord = ws_to_uvw(vs_out.cellCorners[i]);
		vs_out.densityAtCorners[i] = texture(densityTex, texCoord).r;
	}

	uint cubeindex = 0;
	if (vs_out.densityAtCorners[0] <= 0) cubeindex |= 1;
    if (vs_out.densityAtCorners[1] <= 0) cubeindex |= 2;
    if (vs_out.densityAtCorners[2] <= 0) cubeindex |= 4;
    if (vs_out.densityAtCorners[3] <= 0) cubeindex |= 8;
    if (vs_out.densityAtCorners[4] <= 0) cubeindex |= 16;
    if (vs_out.densityAtCorners[5] <= 0) cubeindex |= 32;
    if (vs_out.densityAtCorners[6] <= 0) cubeindex |= 64;
    if (vs_out.densityAtCorners[7] <= 0) cubeindex |= 128;
	
	vs_out.mc_case = cubeindex;
}