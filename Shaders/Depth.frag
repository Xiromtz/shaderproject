#version 330 core

out float depth;

uniform int expIntensity;

void main()
{
	

	depth = exp(expIntensity * gl_FragCoord.z);
}