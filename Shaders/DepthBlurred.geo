﻿#version 430
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VS_OUT
{
	vec3 ws;
} gs_in[];

void main()
{
	for (int i = 0; i < 3; i++)
	{
		gl_Position = vec4(gs_in[i].ws.xz, 0.0, 1.0);
		EmitVertex();
	}
	EndPrimitive();
}