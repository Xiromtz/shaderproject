﻿#version 330 core

in ParticleQuad
{
	flat int type;
	vec2 uv;
} fs_in;

out vec4 FragColor;

uniform sampler2D sprite;

void main()
{
	if (fs_in.type == 1)
		FragColor = (texture(sprite, fs_in.uv) * vec4(0.050, 0.282, 0.827, 1.0));
	else if (fs_in.type == 3)
		FragColor = (texture(sprite, fs_in.uv));
}