﻿#version 330 core
layout(location = 0) in vec3 position;
layout(location = 1) in vec3 velocity;
layout(location = 2) in float lifetime;
layout(location = 3) in uint seed;
layout(location = 4) in int type;

out Particle
{
	flat int type;
	vec3 position;
}vs_out;

void main()
{
	vs_out.position = position;
	vs_out.type = type;
}