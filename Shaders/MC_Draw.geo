#version 430

uniform mat4 model;
uniform mat4 view;

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 pos_fs_in[3];
in vec2 texCoord_fs_in[3];
in vec3 normal_fs_in[3];
out vec3 gFacetNormal;
out vec3 gPatchDistance;
out vec3 gTriDistance;
out vec3 gFragPos;
out vec2 gTexCoords;

void main()
{
	vec3 A = tePosition[2] - tePosition[0];
	vec3 B = tePosition[1] - tePosition[0];
	gFacetNormal = normalize(cross(A, B));

	gPatchDistance = tePatchDistance[0];
	gTriDistance = vec3(1, 0, 0);
	gl_Position = gl_in[0].gl_Position;
	gFragPos = tePosition[0];
	gTexCoords = vec2(gl_Position.x * 1.5, gl_Position.y * 1.5);
	EmitVertex();

	gPatchDistance = tePatchDistance[1];
	gTriDistance = vec3(0, 1, 0);
	gl_Position = gl_in[1].gl_Position;
	gFragPos = tePosition[1];
	gTexCoords = vec2(gl_Position.x * 1.5, gl_Position.y * 1.5);
	EmitVertex();

	gPatchDistance = tePatchDistance[2];
	gTriDistance = vec3(0, 0, 1);
	gl_Position = gl_in[2].gl_Position;
	gFragPos = tePosition[2];
	gTexCoords = vec2(gl_Position.x * 1.5, gl_Position.y * 1.5);
	EmitVertex();

	EndPrimitive();
}