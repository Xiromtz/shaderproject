﻿#version 430
layout(points) in;
layout(triangle_strip, max_vertices = 15) out;

in VS_OUT 
{
	vec3 cellCorners[8];
	float densityAtCorners[8];
	uint mc_case;
} vs_in[];

struct CellTriangles
{
	int tris[16];
};

out Vertex
{
	vec3 wsCoord;
	vec3 normal;
} gs_out;

layout(shared, std140) uniform MC_TrisTable
{
	CellTriangles g_triTable[256];
};

layout (shared, std140) uniform MC_EdgeTable
{
	int edgeTable[256];
};

uniform sampler3D densityTex;

vec3 VertexInterp(vec3 p1, vec3 p2, float valp1, float valp2)
{
	float mu;
	vec3 p;

	if (abs(valp1) < 0.00001)
		return(p1);
	if (abs(valp2) < 0.00001)
		return(p2);
	if (abs(valp1 - valp2) < 0.00001)
		return(p1);

	mu = (-valp1) / (valp2 - valp1);
	p.x = p1.x + mu * (p2.x - p1.x);
	p.y = p1.y + mu * (p2.y - p1.y);
	p.z = p1.z + mu * (p2.z - p1.z);

	return (p);
}

vec3 calculateNormal(vec3 p1, vec3 p2, vec3 p3)
{
	vec3 u = p2 - p1;
	vec3 v = p3 - p1;

	return cross(u, v);
}

void main()
{
	vec3 vertlist[12] = vec3[] (
	vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f),
	vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f));

	if ((edgeTable[vs_in[0].mc_case] & 1) != 0)
		vertlist[ 0] = VertexInterp(vs_in[0].cellCorners[0], vs_in[0].cellCorners[1], vs_in[0].densityAtCorners[0], vs_in[0].densityAtCorners[1]);
	if ((edgeTable[vs_in[0].mc_case] & 2) != 0)
	    vertlist[ 1] = VertexInterp(vs_in[0].cellCorners[1], vs_in[0].cellCorners[2], vs_in[0].densityAtCorners[1], vs_in[0].densityAtCorners[2]);
	if ((edgeTable[vs_in[0].mc_case] & 4) != 0)
	    vertlist[ 2] = VertexInterp(vs_in[0].cellCorners[2], vs_in[0].cellCorners[3], vs_in[0].densityAtCorners[2], vs_in[0].densityAtCorners[3]);
	if ((edgeTable[vs_in[0].mc_case] & 8) != 0)
	    vertlist[ 3] = VertexInterp(vs_in[0].cellCorners[3], vs_in[0].cellCorners[0], vs_in[0].densityAtCorners[3], vs_in[0].densityAtCorners[0]);
	if ((edgeTable[vs_in[0].mc_case] & 16) != 0)
	    vertlist[ 4] = VertexInterp(vs_in[0].cellCorners[4], vs_in[0].cellCorners[5], vs_in[0].densityAtCorners[4], vs_in[0].densityAtCorners[5]);
	if ((edgeTable[vs_in[0].mc_case] & 32) != 0)
	    vertlist[ 5] = VertexInterp(vs_in[0].cellCorners[5], vs_in[0].cellCorners[6], vs_in[0].densityAtCorners[5], vs_in[0].densityAtCorners[6]);
	if ((edgeTable[vs_in[0].mc_case] & 64) != 0)
	    vertlist[ 6] = VertexInterp(vs_in[0].cellCorners[6], vs_in[0].cellCorners[7], vs_in[0].densityAtCorners[6], vs_in[0].densityAtCorners[7]);
	if ((edgeTable[vs_in[0].mc_case] & 128) != 0)
	    vertlist[ 7] = VertexInterp(vs_in[0].cellCorners[7], vs_in[0].cellCorners[4], vs_in[0].densityAtCorners[7], vs_in[0].densityAtCorners[4]);
	if ((edgeTable[vs_in[0].mc_case] & 256) != 0)
	    vertlist[ 8] = VertexInterp(vs_in[0].cellCorners[0], vs_in[0].cellCorners[4], vs_in[0].densityAtCorners[0], vs_in[0].densityAtCorners[4]);
	if ((edgeTable[vs_in[0].mc_case] & 512) != 0)
	    vertlist[ 9] = VertexInterp(vs_in[0].cellCorners[1], vs_in[0].cellCorners[5], vs_in[0].densityAtCorners[1], vs_in[0].densityAtCorners[5]);
	if ((edgeTable[vs_in[0].mc_case] & 1024) != 0)
	    vertlist[10] = VertexInterp(vs_in[0].cellCorners[2], vs_in[0].cellCorners[6], vs_in[0].densityAtCorners[2], vs_in[0].densityAtCorners[6]);
	if ((edgeTable[vs_in[0].mc_case] & 2048) != 0)
	    vertlist[11] = VertexInterp(vs_in[0].cellCorners[3], vs_in[0].cellCorners[7], vs_in[0].densityAtCorners[3], vs_in[0].densityAtCorners[7]);

	for (int i = 0; g_triTable[vs_in[0].mc_case].tris[i] != -1; i += 3)
	{
		vec3 p1 = vertlist[g_triTable[vs_in[0].mc_case].tris[i]];
		vec3 p2 = vertlist[g_triTable[vs_in[0].mc_case].tris[i+1]];
		vec3 p3 = vertlist[g_triTable[vs_in[0].mc_case].tris[i+2]];

		vec3 normal = calculateNormal(p1, p2, p3);

		gs_out.normal = normal;
		gs_out.wsCoord = p1;
		EmitVertex();
		gs_out.normal = normal;
		gs_out.wsCoord = p2;
		EmitVertex();
		gs_out.normal = normal;
		gs_out.wsCoord = p3;
		EmitVertex();
		EndPrimitive();
	}
}