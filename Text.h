#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H
#include <glad/glad.h>
#include <map>
#include <glm/vec2.hpp>
#include "GLShader.h"

class Text
{
private:
    struct Character {
        GLuint     TextureID;  // ID handle of the glyph texture
        glm::ivec2 Size;       // Size of glyph
        glm::ivec2 Bearing;    // Offset from baseline to left/top of glyph
        GLuint     Advance;    // Offset to advance to next glyph
    };
    std::map<GLchar, Character> Characters;

    const char* defaultFont = "fonts/arial.ttf";
    GLuint VAO, VBO;
    GLShader* shader;

    void loadFont(const char* font);
    void setupBuffers();
public:
    Text();
    ~Text();

    void RenderText(std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);
};

