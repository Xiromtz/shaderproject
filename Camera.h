#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include "GLShader.h"
#include <GLFW/glfw3.h>

class Camera
{
private:
	bool rotationSet = false;
	bool lockCamera = false;
	bool cameraMoved = false;

	const float movementSpeed = 0.1f;
	const float mouseSensitivity = 0.001f;

	glm::quat camera_quat;
	glm::quat rotation;
	

	bool hasMouseMoved = false;
	float lastMouseX = 400, lastMouseY = 300;
	float yaw = 0, pitch = 0;

	glm::vec3 eyeVector;
	glm::vec3 velocity;
	glm::vec2 mousePosition;
	
public:
    glm::mat4 view;
	inline void setPos(const glm::vec3 &pos)
	{
		eyeVector = pos;
	}

	inline glm::vec3 getPos () const
	{
		return eyeVector;
	}

	inline void rotate(const glm::quat &rot)
	{
		rotationSet = true;
		rotation = rot;
		updateView();
	}

	inline void lock()
	{
		lockCamera = true;
	}

	void ProcessKeyboard(GLFWwindow* window);
	void mouseRotate(double xPos, double yPos);
	void update();
	void draw(const GLShader &shader);
	void updateView();
    void setLookAtVals(glm::vec3& cameraPos, glm::vec3& lookAt, float& tMax, const glm::mat4& proj);

	Camera();
	~Camera();
};

