#include "Texture3dCascades.h"



Texture3dCascades::Texture3dCascades() : texture(WIDTH, HEIGHT, DEPTH, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE,
    GL_NEAREST, GL_NEAREST, GL_TEXTURE0, GL_R16F, GL_RED, GL_FLOAT), shader("Shaders/Texture3d.vert", "Shaders/Texture3d.geo", "Shaders/Texture3d.frag")
{
    fbo.bindTexture(texture);

    VertexAttributes vAttrs(vertices, sizeof(vertices), nullptr, 0);
    vAttrs.set_2d_coordinate_attributes();

    vbo = new VBO(vAttrs);
    vao = new VAO(*vbo, &vAttrs);
}


Texture3dCascades::~Texture3dCascades()
{
    delete vbo;
    delete vao;
}

void Texture3dCascades::draw()
{
    fbo.bind();
    
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
    glReadBuffer(GL_NONE);

    glViewport(0, 0, WIDTH, HEIGHT);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND); 

    shader.use();
    
    vao->bind();
    glDrawArraysInstanced(GL_TRIANGLES, 0, 6, DEPTH);
    vao->unbind();

    fbo.unbind();
}

void Texture3dCascades::bind() const
{
    texture.bind();
}

void Texture3dCascades::unbind() const
{
    texture.unbind();
}


Texture Texture3dCascades::get3dTexture()
{
    return texture;
}
