#include "FBO.h"

FBO::FBO()
{
    glGenFramebuffers(1, &ID);
}

FBO::~FBO()
{
    glDeleteFramebuffers(1, &ID);
}

void FBO::bindTexture(const Texture& texture)
{
    bind();

    glBindTexture(GL_TEXTURE_3D, texture.ID);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture.ID, 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        throw std::runtime_error("Error while creating framebuffer!");
    }

    texture.unbind();

    unbind();
}


void FBO::bind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, ID);
}

void FBO::unbind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
