#pragma once

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include "GLObject.h"
#include <glm/ext.hpp>


class Triangle : public GLObject
{
private:
    float vertices[9];
    unsigned int indices[3] = { 0, 1, 2 };
public:
    glm::vec3 points[3];

    bool ignore = false;

    explicit Triangle(){}
    Triangle(const Triangle& old)
    {
        points[0] = old.points[0];
        points[1] = old.points[1];
        points[2] = old.points[2];
        setup();
        init();
    }
    explicit Triangle(glm::vec3 a, glm::vec3 b, glm::vec3 c);
    ~Triangle();

    void setup();
    void createVertices();
    void draw(const GLShader& shader) const override;
    std::vector<Triangle*> getTriangles() const override { return std::vector<Triangle*>(); }
    bool intersects(const glm::vec3& a, const glm::vec3& d, glm::vec3& intersectionPoint) const;

    friend bool operator==(const Triangle& lhs, const Triangle& rhs)
    {
        return lhs.points[0] == rhs.points[0] && lhs.points[1] == rhs.points[1] && lhs.points[2] == rhs.points[2];
    }
};

