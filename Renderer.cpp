#include "Renderer.h"
#include "DisplacedQuad.h"
#include "Cube.h"
#include "Floor.h"

Renderer::Renderer()
{
	shadowShader = new GLShader("Shaders/Shadow.vert", "Shaders/Shadow.frag");
	greenShader = new GLShader("Shaders/Color.vert", "Shaders/Color.frag");
    displacementShader = new GLShader("Shaders/Displacement.vert", "Shaders/Displacement.frag");
    mc_drawShader = new GLShader("Shaders/MC_Draw.vert", "Shaders/MC_Draw.tc", "Shaders/MC_Draw.te", "Shaders/MC_Draw.geo", "Shaders/MC_Draw.frag");
    lightShader = new GLShader("Shaders/Lighted.vert", "Shaders/Lighted.frag");
    renderParticles = new GLShader("Shaders/RenderParticles.vert", "Shaders/RenderParticles.geo",  "Shaders/RenderParticles.frag");
    camera = new CameraAlt();
    particleSystem = new ParticleSystem();
	setup();
}

void Renderer::setup()
{
    cube = new Cube("Texture/cube1.png", "Texture/cube1_normal.png");
    cube->translate(glm::vec3(10, 0, 0));
    cube->update();
    cube->setTriangles();
    objects[lightShader].push_back(cube);

    Floor* floor = new Floor();
    floor->translate(glm::vec3(0.0f, -3.5f, -10.0f));
    floor->update();
    floor->setTriangles();
    objects[shadowShader].push_back(floor);

    DisplacedQuad* displacedQuad = new DisplacedQuad();
    displacedQuad->translate(glm::vec3(5, 0, -5));
    displacedQuad->update();
    objects[displacementShader].push_back(displacedQuad);

	lightPos = glm::vec3(-10.0f, 20.0f, -15.0f);

	lightUp = glm::vec3(0.0f, 1.0f, 0.0f);
	lightProjection = glm::ortho(-30.0f, 30.0f, -30.0f, 30.0f, 1.0f, 50.0f);
	glm::mat4 lightView = glm::lookAt(lightPos,
		glm::vec3(0,0,0),
		lightUp);
	lightMatrix = lightProjection * lightView;

	projection = glm::mat4(1.0f);
	projection = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 0.1f, 100.0f);
	depthMap = new DepthMap();
    blurredDepthMap = new BlurredDepthMap(1024, 1024);

    displacementShader->use();
    displacementShader->setInt("diffuseMap", 0);
    displacementShader->setInt("normalMap", 1);
    displacementShader->setInt("depthMap", 2);
    displacementShader->setFloat("height_scale", 0.1f);
    displacementShader->setFloat("numLayers", numDisplacementLayers);
    displacementShader->setFloat("secondNumLayers", numSecondDisplacementLayers);
    displacementShader->setMatrix("projection", projection);
    displacementShader->setVec3("lightPos", lightPos);
    displacementShader->setVec3("viewPos", camera->Position);

	shadowShader->use();
    shadowShader->setInt("expIntensity", expIntensity);
	shadowShader->setInt("diffuseTexture", 0);
	shadowShader->setInt("shadowMap", 1);
	shadowShader->setInt("normalMap", 2);
	shadowShader->setFloat("normalIntensity", 1.0f);
	shadowShader->setMatrix("projection", projection);
    
	greenShader->use();
	greenShader->setMatrix("projection", projection);

    mc_drawShader->use();
    mc_drawShader->setInt("texture1", 0);
    mc_drawShader->setInt("texture2", 1);
    mc_drawShader->setInt("texture3", 2);
    mc_drawShader->setMatrix("projection", projection);
    mc_drawShader->setVec3("lightPos", lightPos);

    lightShader->use();
    lightShader->setMatrix("projection", projection);
    lightShader->setInt("diffuseTexture", 0);
    lightShader->setVec3("lightPos", lightPos);

    renderParticles->use();
    renderParticles->setMatrix("projection", projection);
    renderParticles->setInt("sprite", 0);
}

Renderer::~Renderer()
{
	for (auto it = objects.begin(); it != objects.end(); ++it)
	{
		std::vector<GLObject*> current = it->second;
		for (unsigned int i = 0; i < current.size(); i++)
		{
			delete current[i];
		}
	}
	delete shadowShader;
	delete greenShader;
    delete mc_drawShader;
	delete camera;
	delete depthMap;
}

void Renderer::render(GLFWwindow* window, size_t framesPerSecond)
{	
    if (shouldGenerate3dTexture)
    {
        texture3d.draw();
        marchingCubes.drawToBuffer(texture3d);
        mc_geometry = new MC_Geometry(marchingCubes.geometry, marchingCubes.size);
        objects[mc_drawShader].push_back(mc_geometry);
        shouldGenerate3dTexture = false;
    }

    renderDepthMap();
    blurredDepthMap->draw(depthMap->depthMap);

    glEnable(GL_DEPTH_TEST);
    

	glViewport(0, 0, 800, 600);
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    text.RenderText(std::to_string(numDisplacementLayers) + "/" + std::to_string(numSecondDisplacementLayers), 25.0f, 25.0f, 1.0f, glm::vec3(0.5f, 0.8f, 0.2f));
    text.RenderText("FPS: " + std::to_string(framesPerSecond), 25.0f, 500.0f, 1.0f, glm::vec3(0.5f, 0.8f, 0.2f));
    text.RenderText("ESM k: " + std::to_string(expIntensity), 25.0f, 400.0f, 1.0f, glm::vec3(0.5f, 0.8f, 0.2f));
    text.RenderText("Tessellation Level: " + std::to_string(tessellationLevel), 25.0f, 300.0f, 1.0f, glm::vec3(0.5f, 0.8f, 0.2f));

    renderDisplacementObjects();
	renderShadowObjects();
	
    renderMCObjects();
    renderGreenObjects();
    renderParticlesFunc();
    renderLightedObjects();

	glfwSwapBuffers(window);
	glfwPollEvents();
}

void Renderer::renderDisplacementObjects()
{
    displacementShader->use();

    displacementShader->setFloat("numLayers", numDisplacementLayers);
    displacementShader->setFloat("secondNumLayers", numSecondDisplacementLayers);

    displacementShader->setMatrix("view", camera->GetViewMatrix());
    displacementShader->setVec3("viewPos", camera->Position);

    std::vector<GLObject*> displacementObjects = objects[displacementShader];
    for (size_t i = 0; i < displacementObjects.size(); i++)
    {
        displacementObjects[i]->draw(*displacementShader);
    }
}


void Renderer::renderDepthMap() const
{
	depthMap->bind();
	depthMap->shader->setMatrix("lightSpaceMatrix", lightMatrix);
    depthMap->shader->setInt("expIntensity", expIntensity);

	for (auto it = objects.begin(); it != objects.end(); ++it)
	{
		std::vector<GLObject*> current = it->second;
		for (int i = 0; i < current.size(); i++)
			current[i]->draw(*depthMap->shader);
	}

	depthMap->unbind();
}

void Renderer::renderShadowObjects()
{
	shadowShader->use();

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, blurredDepthMap->blurredDepthmapID);
	
    shadowShader->setMatrix("view", camera->GetViewMatrix());
	shadowShader->setVec3("viewPos", camera->Position);
	shadowShader->setVec3("lightPos", mixedLightPos);
	shadowShader->setMatrix("lightSpaceMatrix", lightMatrix);
	shadowShader->setBool("useLighting", useLighting);
    shadowShader->setInt("expIntensity", expIntensity);

	std::vector<GLObject*> shadowObjects = objects[shadowShader];
	for (unsigned int i = 0; i < shadowObjects.size(); i++)
	{
		shadowObjects[i]->draw(*shadowShader);
	}
}

void Renderer::renderGreenObjects()
{
	greenShader->use();
    greenShader->setMatrix("view", camera->GetViewMatrix());

    currentlyIntersecting = mc_geometry->getClosestIntersectingTriangle(camera->Position, glm::normalize(camera->Front));
    if (currentlyIntersecting != nullptr)
        currentlyIntersecting->draw(*greenShader);

	std::vector<GLObject*> greenObjects = objects[greenShader];
	for (unsigned int i = 0; i < greenObjects.size(); i++)
	{
		greenObjects[i]->draw(*greenShader);
	}
}

void Renderer::renderMCObjects()
{
    if (showWireframe)
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    mc_drawShader->use();

    mc_drawShader->setFloat("TessLevelInner", tessellationLevel);
    mc_drawShader->setFloat("TessLevelOuter", tessellationLevel);
    mc_drawShader->setMatrix("view", camera->GetViewMatrix());

    mc_drawShader->setVec3("viewPos", camera->Position);

    std::vector<GLObject*> mcObjects = objects[mc_drawShader];
    for (unsigned int i = 0; i < mcObjects.size(); i++)
    {
        mcObjects[i]->draw(*mc_drawShader);
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Renderer::renderLightedObjects()
{
    lightShader->use();
    lightShader->setMatrix("view", camera->GetViewMatrix());

    lightShader->setVec3("viewPos", camera->Position);

    std::vector<GLObject*> lightedObjects = objects[lightShader];
    for (unsigned int i = 0; i < lightedObjects.size(); i++)
    {
        lightedObjects[i]->draw(*lightShader);
    }
}

void Renderer::renderParticlesFunc()
{
    renderParticles->use();
    renderParticles->setMatrix("view", camera->GetViewMatrix());
    particleSystem->draw(*renderParticles);
}
