#include "VBO.h"
#include "VAO.h"
#include "EBO.h"

VBO::VBO(const VertexAttributes &attrs)
{
	glGenBuffers(1, &ID);
	bind();
	glBufferData(GL_ARRAY_BUFFER, attrs.vertexArraySize, attrs.vertices, GL_STATIC_DRAW);
	unbind();
}

VBO::~VBO()
{
	glDeleteBuffers(1, &ID);
}
