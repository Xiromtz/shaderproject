#pragma once
#include <glad/glad.h>
#include <vector>
#include "Particle.h"
#include "GLShader.h"
#include "Texture.h"
#include "GLObject.h"

class ParticleSystem
{
private:
    std::vector<GLchar*> feedbackVaryings = { "gs_out.position", "gs_out.velocity", "gs_out.lifetime", "gs_out.seed", "gs_out.type"};
    GLShader* updateShader;

    struct Particle
    {
        glm::vec3 position;
        glm::vec3 velocity;
        float lifetime;
        uint32_t seed;
        int type;
    };

    GLuint VBO[2], VAO[2];
    GLuint readBuffer = 0, writeBuffer = 1;

    Texture* particleTexture;

    const GLuint MaxParticles = 10000;
    GLuint nr_particles = 0;

    GLuint lastUsedParticle = 0;

public:
    ParticleSystem();
    ~ParticleSystem();

    void update(float deltaTime);
    void draw(const GLShader& shader);
    void addEmitter(const glm::vec3& pos, int type);
};

