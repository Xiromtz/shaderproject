#include "KD_Tree.h"
#include <iostream>


KD_Tree::KD_Tree(const std::vector<glm::vec3>& points)
{
    unorderedTree = std::vector<node>(points.size());
    for (size_t i = 0u; i < unorderedTree.size(); i++)
    {
        unorderedTree[i] = { points[i], -1, nullptr, nullptr };
    }

    root = make_tree(unorderedTree.data(), unorderedTree.size(), 0, 3);
}

node* KD_Tree::make_tree(node* t, int len, int i, int dim)
{
    node* n;
    if (!len)
        return nullptr;

    if ((n = find_median(t, t + len, i)))
    {
        n->splitAxis = i;
        i = (i + 1) % dim;
        
        n->left = make_tree(t, n - t, i, dim);
        n->right = make_tree(n + 1, t + len - (n + 1), i, dim);
    }
    return n;
}


double KD_Tree::dist(node* a, node* b, int dim)
{
    double t, d = 0;
    while (dim--)
    {
        t = a->point[dim] - b->point[dim];
        d += t * t;
    }
    return d;
}

void KD_Tree::swap(node* x, node* y)
{
    glm::vec3 tmp;
    tmp = x->point;
    x->point = y->point;
    y->point = tmp;
}

node* KD_Tree::find_median(node* start, node* end, int idx)
{
    if (end <= start)
        return nullptr;

    if (end == start + 1)
        return start;

    node* p, *store, *md = start + (end - start) / 2;

    double pivot;
    while (true)
    {
        pivot = md->point[idx];
        swap(md, end - 1);
        for (store = p = start; p < end; p++)
        {
            if (p->point[idx] < pivot)
            {
                if (p != store)
                    swap(p, store);
                store++;
            }
        }
        swap(store, end - 1);

        if (store->point[idx] == md->point[idx])
            return md;

        if (store > md)
            end = store;
        else
            start = store;
    }
}


node* KD_Tree::getClosestNodeAlongRay(node* current, const glm::vec3& a, const glm::vec3& d, float tMax, node* closest)
{
    if (current == nullptr)
        return closest;

    if (closest == nullptr || glm::distance(a, closest->point) > glm::distance(a, current->point))
        closest = current;

    int dim = current->splitAxis;
    int first = a[dim] > current->point[dim];

    if (d[dim] == 0.0f)
    {
        closest = getClosestNodeAlongRay(first == 0 ? current->left : current->right, a, d, tMax, closest);
    }
    else
    {
        float t = (current->point[dim] - a[dim]) / d[dim];

        if (0.0f <= t && t < tMax)
        {
            closest = getClosestNodeAlongRay(first == 0 ? current->left : current->right, a, d, t, closest);
            closest = getClosestNodeAlongRay(first == 0 ? current->right : current->left, a + t * d, d, tMax - t, closest);
        }
        else
        {
            closest = getClosestNodeAlongRay(first == 0 ? current->left : current->right, a, d, tMax, closest);
        }
    }

    return closest;
}


KD_Tree::~KD_Tree()
{
}
