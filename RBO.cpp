#include "RBO.h"

RBO::RBO()
{
    glGenRenderbuffers(1, &ID);
}

void RBO::bindToFBO(GLuint fbo)
{
    bind();

    glRenderbufferStorage(GL_RENDERBUFFER, GL_R16F, 96, 96);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, ID);


}


void RBO::bind()
{
    glBindRenderbuffer(GL_RENDERBUFFER, ID);
}

void RBO::unbind()
{
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
}


RBO::~RBO()
{
    
}
