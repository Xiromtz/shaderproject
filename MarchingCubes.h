#pragma once
#include "GLShader.h"
#include "VAO.h"
#include "MarchingCubesLookupTable.h"
#include "Texture3dCascades.h"

class MarchingCubes
{
private:
    GLShader* mc_shader;
    MarchingCubesLookupTable lookupTable;
    std::vector<GLchar*> feedbackVaryings = { "Vertex.wsCoord", "Vertex.normal" };
    glm::vec2 vertices[95*95];

    GLuint query;
    GLuint generatedVertexBufferObject;

    VAO* vao;
    VBO* vbo;

    void generateVertices();
    void generateBuffers();

    void saveBufferValues();
public:
    size_t size;
    glm::vec3* geometry;

    explicit MarchingCubes();
    ~MarchingCubes();

    void drawToBuffer(const Texture3dCascades& texture);
};

