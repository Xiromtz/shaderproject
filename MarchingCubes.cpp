#include "MarchingCubes.h"

MarchingCubes::MarchingCubes()
{
    mc_shader = new GLShader("Shaders/MarchingCubes.vert", "Shaders/MarchingCubes.geo", feedbackVaryings);

    generateVertices();
    generateBuffers();

    glGenQueries(1, &query);
    lookupTable.BindTablesToShader(*mc_shader);
    
}

void MarchingCubes::generateVertices()
{

    float currentX = -1.0f;
    float currentY = -1.0f;
    float stepSize = 2.0f / 95.0f;

    for (size_t i = 0; i < 95 * 95; i ++)
    {
        if (i != 0 && i % 95 == 0)
        {
            currentX = -1.0f;
            currentY += stepSize;
        }

        vertices[i] = glm::vec2(currentX, currentY);
        currentX += stepSize;
    }
}

void MarchingCubes::generateBuffers()
{
    VertexAttributes vAttrs(reinterpret_cast<float*>(vertices), sizeof(vertices), nullptr, 0);
    vAttrs.set_2d_coordinate_attributes();

    vbo = new VBO(vAttrs);
    vao = new VAO(*vbo, &vAttrs);
    
    glGenBuffers(1, &generatedVertexBufferObject);
    glBindBuffer(GL_ARRAY_BUFFER, generatedVertexBufferObject);
    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 2 * 95 * 95 * 255 * 15, nullptr, GL_STATIC_READ);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void MarchingCubes::drawToBuffer(const Texture3dCascades& texture)
{
    
    mc_shader->use();

    texture.bind();
    vao->bind();

    glEnable(GL_RASTERIZER_DISCARD);
    glBindBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, generatedVertexBufferObject, 0, sizeof(glm::vec3) * 2 * 95 * 95 * 255 * 15);
    
    glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
    glBeginTransformFeedback(GL_TRIANGLES);

        glDrawArraysInstanced(GL_POINTS, 0, 95 * 95, 255);

    glEndTransformFeedback();
    glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
    glDisable(GL_RASTERIZER_DISCARD);
    glFlush();

    vao->unbind();
    texture.unbind();

    saveBufferValues();
}

void MarchingCubes::saveBufferValues()
{
    GLuint primitives;
    glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitives);

    size = primitives * 3 * 2;
    geometry = new glm::vec3[size];
	std::vector<glm::vec3> geometryVec(size);
    glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(glm::vec3) * size, geometry);
}


MarchingCubes::~MarchingCubes()
{
    delete vbo;
    delete vao;
    delete mc_shader;
    delete geometry;
}