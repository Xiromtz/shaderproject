#include "ParticleSystem.h"
#include <chrono>

ParticleSystem::ParticleSystem()
{
    particleTexture = new Texture("Texture/particle_01.png", GL_REPEAT, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, GL_TEXTURE0, GL_RGBA, GL_UNSIGNED_BYTE);
    updateShader = new GLShader("Shaders/UpdateParticles.vert", "Shaders/UpdateParticles.geo", feedbackVaryings);

    glGenVertexArrays(2, VAO);
    glGenBuffers(2, VBO);

    for (int i = 0; i < 2; i++)
    {
        glBindVertexArray(VAO[i]);
        glBindBuffer(GL_ARRAY_BUFFER, VBO[i]);

        //Position
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), static_cast<GLvoid*>(nullptr));
        glEnableVertexAttribArray(0);
        //Velocity
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), reinterpret_cast<GLvoid*>(sizeof(glm::vec3)));
        glEnableVertexAttribArray(1);
        //Lifetime
        glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), reinterpret_cast<GLvoid*>(sizeof(glm::vec3) * 2));
        glEnableVertexAttribArray(2);
        //Seed
        glVertexAttribIPointer(3, 1, GL_UNSIGNED_INT, sizeof(Particle), reinterpret_cast<GLvoid*>(sizeof(glm::vec3) * 2 + sizeof(uint32_t)));
        glEnableVertexAttribArray(3);
        //Type
        glVertexAttribIPointer(4, 1, GL_INT, sizeof(Particle), reinterpret_cast<GLvoid*>(sizeof(glm::vec3) * 2 + sizeof(uint32_t) + sizeof(int)));
        glEnableVertexAttribArray(4);

        
    }
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    for (int i = 0; i < 2; i++)
    {
        glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, VBO[i]);
        glBufferData(GL_TRANSFORM_FEEDBACK_BUFFER, MaxParticles * sizeof(Particle), nullptr, GL_STREAM_COPY);
    }
    glBindBuffer(GL_TRANSFORM_FEEDBACK_BUFFER, 0);
}


ParticleSystem::~ParticleSystem()
{
    delete particleTexture;
    delete updateShader;
}

void ParticleSystem::update(float deltaTime)
{
    if (nr_particles == 0)
        return;

    updateShader->use();
    updateShader->setFloat("deltaTime", deltaTime);

    GLuint query;
    glGenQueries(1, &query);

    glEnable(GL_RASTERIZER_DISCARD);

    glBindVertexArray(VAO[readBuffer]);
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, VBO[writeBuffer]);

    glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
    glBeginTransformFeedback(GL_POINTS);

    glDrawArrays(GL_POINTS, 0, nr_particles);

    glEndTransformFeedback();
    glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);

    glBindVertexArray(0);

    /*std::vector<Particle> particlesBefore(nr_particles);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[readBuffer]);
    glGetBufferSubData(GL_ARRAY_BUFFER, 0, nr_particles * sizeof(Particle), particlesBefore.data());*/

    glGetQueryObjectuiv(query, GL_QUERY_RESULT, &nr_particles);
    std::cout << "Current amount of particles: " << nr_particles << std::endl;
    
    glFlush();
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);
    
    /*if (nr_particles > 0)
    {
        std::vector<Particle> particlesAfter(nr_particles);
        glBindBuffer(GL_ARRAY_BUFFER, VBO[writeBuffer]);
        glGetBufferSubData(GL_ARRAY_BUFFER, 0, nr_particles * sizeof(Particle), particlesAfter.data());
    }*/
    glDisable(GL_RASTERIZER_DISCARD);
    glDeleteQueries(1, &query);
    
    readBuffer = 1 - readBuffer;
    writeBuffer = 1 - writeBuffer;
}

void ParticleSystem::addEmitter(const glm::vec3& pos, int type)
{
    if (nr_particles == MaxParticles)
        return;

    Particle emitter;
    emitter.position = pos;
    emitter.lifetime = 0.0f;
    emitter.velocity = glm::vec3(0);
    emitter.seed = static_cast<uint32_t>(time(nullptr));
    emitter.type = type;

    glBindBuffer(GL_ARRAY_BUFFER, VBO[readBuffer]);
    glBufferSubData(GL_ARRAY_BUFFER, nr_particles * sizeof(Particle), sizeof(Particle), &emitter);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    ++nr_particles;
}


void ParticleSystem::draw(const GLShader& shader)
{
    if (nr_particles == 0)
        return;

    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    
    
    particleTexture->bind();
    glBindVertexArray(VAO[readBuffer]);
    glDrawArrays(GL_POINTS, 0, nr_particles);
    glBindVertexArray(0);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
