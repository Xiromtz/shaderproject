#pragma once

#include <glad/glad.h>
#include "VBO.h"
#include "VertexAttributes.h"

class VAO
{
public:
	GLuint ID;
	VAO(const VBO& vbo, VertexAttributes* attrs);
	~VAO();

	inline void bind() const
	{
		glBindVertexArray(ID);
	}

	inline void unbind() const
	{
		glBindVertexArray(0);
	}
};

