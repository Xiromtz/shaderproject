#include "Floor.h"

Floor::Floor()
{
    setup();
}

Floor::Floor(const char* texturePath, const char* normalTexturePath) : texturePath(texturePath), normalTexturePath(normalTexturePath)
{
    setup();
}

void Floor::setup()
{
    addTangents();
    vAttrs = new VertexAttributes(vertices, 44 * sizeof(float), indices, sizeof(this->indices));
    vAttrs->set_coordinate_normal_texture_tangent_attributes();
    tex = new Texture(texturePath, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR, GL_TEXTURE0, GL_RGB, GL_UNSIGNED_BYTE);
    normalTex = new Texture(normalTexturePath, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR, GL_TEXTURE2, GL_RGB, GL_UNSIGNED_BYTE);
    init();
}


void Floor::addTangents()
{
    float* tempVertices = new float[44];
    int vertexStartIndex = 0;
    glm::vec3 pos1(vertices[vertexStartIndex], vertices[vertexStartIndex + 1], vertices[vertexStartIndex + 2]);
    glm::vec2 uv1(vertices[vertexStartIndex + 6], vertices[vertexStartIndex + 7]);
    glm::vec3 pos2(vertices[vertexStartIndex + 8], vertices[vertexStartIndex + 9], vertices[vertexStartIndex + 10]);
    glm::vec2 uv2(vertices[vertexStartIndex + 14], vertices[vertexStartIndex + 15]);
    glm::vec3 pos3(vertices[vertexStartIndex + 16], vertices[vertexStartIndex + 17], vertices[vertexStartIndex + 18]);
    glm::vec2 uv3(vertices[vertexStartIndex + 22], vertices[vertexStartIndex + 23]);

    glm::vec3 edge1 = pos2 - pos1;
    glm::vec3 edge2 = pos3 - pos1;
    glm::vec2 deltaUV1 = uv2 - uv1;
    glm::vec2 deltaUV2 = uv3 - uv1;

    glm::vec3 tangent;

    float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

    tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
    tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
    tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
    tangent = glm::normalize(tangent);

    int tempIndex = 0;
    for (int o = vertexStartIndex; o < vertexStartIndex + 32; o += 8)
    {
        for (int u = o; u < o + 8; u++)
        {
            tempVertices[tempIndex] = vertices[u];
            tempIndex++;
        }
        tempVertices[tempIndex] = tangent.x;
        tempVertices[tempIndex + 1] = tangent.y;
        tempVertices[tempIndex + 2] = tangent.z;
        tempIndex += 3;
    }

    delete[] vertices;
    vertices = tempVertices;
}


Floor::~Floor()
{
    delete normalTex;
    delete[] vertices;
    for (auto it = triangles.begin(); it != triangles.end(); ++it)
    {
        delete *it;
    }
}

void Floor::update()
{
    GLObject::update();
}

void Floor::draw(const GLShader& shader) const
{
    tex->bind();
    normalTex->bind();
    vao->bind();

    shader.setMatrix("model", model);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, indices);

    vao->unbind();
    normalTex->unbind();
    tex->unbind();
}

void Floor::setTriangles()
{
    int vertexWidth = 11;
    for (int i = 0; i < 6;)
    {
        int posVec1 = indices[i] * vertexWidth;
        int posVec2 = indices[i + 1] * vertexWidth;
        int posVec3 = indices[i + 2] * vertexWidth;

        glm::vec3 vec1(vertices[posVec1], vertices[posVec1 + 1], vertices[posVec1 + 2]);
        glm::vec3 vec2(vertices[posVec2], vertices[posVec2 + 1], vertices[posVec2 + 2]);
        glm::vec3 vec3(vertices[posVec3], vertices[posVec3 + 1], vertices[posVec3 + 2]);

        vec1 = model * glm::vec4(vec1, 1.0f);
        vec2 = model * glm::vec4(vec2, 1.0f);
        vec3 = model * glm::vec4(vec3, 1.0f);

        Triangle* triangle = new Triangle(vec1, vec2, vec3);
        triangles.push_back(triangle);

        i += 3;
    }
}


std::vector<Triangle*> Floor::getTriangles() const
{
    return triangles;
}
