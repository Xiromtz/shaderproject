#pragma once
#include "GLShader.h"
#include "Camera.h"
#include "GLObject.h"
#include "DepthMap.h"
#include <unordered_map>
#include "Texture3dCascades.h"
#include "MarchingCubes.h"
#include "MC_Geometry.h"
#include "Text.h"
#include "Cube.h"
#include "CameraAlt.h"
#include "ParticleSystem.h"
#include <chrono>
#include "BlurredDepthMap.h"

class Renderer
{
private:
	GLShader* shadowShader;
	GLShader* greenShader;
    GLShader* displacementShader;
    GLShader* mc_drawShader;
    GLShader* lightShader;
    GLShader* renderParticles;
	DepthMap* depthMap;
    BlurredDepthMap* blurredDepthMap;

    Text text;
    Triangle* currentlyIntersecting = nullptr;

    Texture3dCascades texture3d;
    MarchingCubes marchingCubes;
    MC_Geometry* mc_geometry;
    Cube* cube;
    bool shouldGenerate3dTexture = true;

    glm::mat4 projection;
	glm::mat4 lightMatrix;
	glm::mat4 lightProjection;
	glm::vec3 lightPos;
	glm::vec3 otherLightPos;
	glm::vec3 mixedLightPos;
	float lightPosMix = 0.0f;
	bool useLighting = true;
	float currentBumpiness = 1.0f;
	glm::vec3 lightUp;
	glm::vec3 lightLookAt;
    float numDisplacementLayers = 10.0f;
    float numSecondDisplacementLayers = 5.0f;
    int expIntensity = 80;
    bool showWireframe = false;
    int tessellationLevel = 1;

	void setup();
	
public:
    ParticleSystem* particleSystem;
	std::unordered_map<GLShader*, std::vector<GLObject*>> objects;
	//Camera* camera;
    CameraAlt* camera;
	Renderer();
	~Renderer();
	void render(GLFWwindow* window, size_t framesPerSecond);
	void renderDepthMap() const;
	void renderShadowObjects();
	void renderGreenObjects();
    void renderMCObjects();
    void renderDisplacementObjects();
    void renderLightedObjects();
    void renderParticlesFunc();

    void toggleWireframe()
    {
        showWireframe = !showWireframe;
    }

    void addTessellationLevel()
    {
        tessellationLevel++;
    }

    void removeTessellationLevel()
    {
        if (tessellationLevel > 1)
            tessellationLevel--;
    }

	void toggleLighting()
	{
		useLighting = !useLighting;
	}
	
    void addDisplacementLayer()
	{
        numDisplacementLayers++;
	}

    void removeDisplacentLayer()
	{
        if (numDisplacementLayers > 1.0f)
            numDisplacementLayers--;
	}

    void addSecondaryDisplacementLayer()
	{
        numSecondDisplacementLayers++;
	}

    void removeSecondaryDisplacementLayer()
	{
        if (numSecondDisplacementLayers > 1.0f)
            numSecondDisplacementLayers--;
	}

    void addESMIntensity()
	{
        expIntensity += 10;
	}

    void removeESMIntensity()
	{
        if (expIntensity > 10)
            expIntensity -= 10;
	}

    void addRainParticleEmitter()
	{
	    if (currentlyIntersecting != nullptr)
	    {
            glm::vec3 centerOfTriangle = (currentlyIntersecting->points[0] + currentlyIntersecting->points[1] + currentlyIntersecting->points[2]) / 3.0;
            particleSystem->addEmitter(centerOfTriangle, 0);
	    }
	}

    void addGasParticleEmitter()
    {
        if (currentlyIntersecting != nullptr)
        {
            glm::vec3 centerOfTriangle = (currentlyIntersecting->points[0] + currentlyIntersecting->points[1] + currentlyIntersecting->points[2]) / 3.0;
            particleSystem->addEmitter(centerOfTriangle, 2);
        }
    }
};

