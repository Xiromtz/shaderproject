// EZGRProjekt.cpp : Defines the entry point for the console application.
//
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "Renderer.h"
#include "Camera.h"

Renderer* renderer;

bool lightingToggled = false;
bool gridToggled = false;
bool userExit = false;
bool reset = false;
bool hasTakenChangeDisplacementLayerInput = false;
bool hasTakenChangeSecondaryDisplacementLayerInput = false;
bool hasTakenESMInput = false;
bool hasTakenTessInput = false;

GLFWwindow* setupWindow();
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void processInput(GLFWwindow* window);
void loop(GLFWwindow* window);

void APIENTRY glDebugOutput(GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar *message,
    const void *userParam)
{
    // ignore non-significant error/warning codes
    if (id == 131169 || id == 131185 || id == 131218 || id == 131204) return;

    std::cout << "---------------" << std::endl;
    std::cout << "Debug message (" << id << "): " << message << std::endl;

    switch (source)
    {
    case GL_DEBUG_SOURCE_API:             std::cout << "Source: API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   std::cout << "Source: Window System"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "Source: Shader Compiler"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY:     std::cout << "Source: Third Party"; break;
    case GL_DEBUG_SOURCE_APPLICATION:     std::cout << "Source: Application"; break;
    case GL_DEBUG_SOURCE_OTHER:           std::cout << "Source: Other"; break;
    } std::cout << std::endl;

    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR:               std::cout << "Type: Error"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "Type: Deprecated Behaviour"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "Type: Undefined Behaviour"; break;
    case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "Type: Portability"; break;
    case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "Type: Performance"; break;
    case GL_DEBUG_TYPE_MARKER:              std::cout << "Type: Marker"; break;
    case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "Type: Push Group"; break;
    case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "Type: Pop Group"; break;
    case GL_DEBUG_TYPE_OTHER:               std::cout << "Type: Other"; break;
    } std::cout << std::endl;

    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:         std::cout << "Severity: high"; break;
    case GL_DEBUG_SEVERITY_MEDIUM:       std::cout << "Severity: medium"; break;
    case GL_DEBUG_SEVERITY_LOW:          std::cout << "Severity: low"; break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "Severity: notification"; break;
    } std::cout << std::endl;
    std::cout << std::endl;
}

int main()
{
  while (!userExit)
  {
    reset = false;
    glfwInit();
    GLFWwindow* window = setupWindow();
    if (window == nullptr)
      return -1;

    loop(window);

    glfwTerminate();
    delete renderer;
  }
	return 0;
}

GLFWwindow* setupWindow()
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4.3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return nullptr;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetCursorPosCallback(window, mouse_callback);

	if (!gladLoadGLLoader(reinterpret_cast<GLADloadproc>(glfwGetProcAddress)))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return nullptr;
	}

	glViewport(0, 0, 800, 600);
	glEnable(GL_DEPTH_TEST);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    GLint flags; glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
    if (flags & GL_CONTEXT_FLAG_DEBUG_BIT)
    {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(glDebugOutput, nullptr);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
    }

	return window;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}


void loop(GLFWwindow* window)
{
	renderer = new Renderer();

	double sec_per_update = 0.0166;

	double previous = glfwGetTime();
	double lag = 0.0;

    size_t framesPerSecond = 0;
    size_t framesCounted = 0;
    double oneSecond = 0.0f;

	while (!glfwWindowShouldClose(window))
	{
		double current = glfwGetTime();
		double elapsed = current - previous;
        oneSecond += elapsed;

        if (oneSecond >= 1.0f)
        {
            framesPerSecond = framesCounted;
            framesCounted = 0;
            oneSecond = 0.0f;
        }

		previous = current;
		lag += elapsed;
		
		processInput(window);
        if (reset)
            break;

        float updateTimePassed = 0;
 		while (lag >= sec_per_update)
		{
			std::unordered_map<GLShader*, std::vector<GLObject*>> objects = renderer->objects;
			for (auto it = objects.begin(); it != objects.end(); ++it)
			{
				std::vector<GLObject*> current = it->second;
				for (unsigned int i = 0; i < current.size(); i++)
				{
					current[i]->update();
				}
			}

			lag -= sec_per_update;
            updateTimePassed += sec_per_update;
		}

        if (updateTimePassed > 0)
        {
            renderer->particleSystem->update(updateTimePassed);
        }
		
        
		renderer->render(window, framesPerSecond);
	    framesCounted++;
	}

  if (!reset)
    userExit = true;
}

void processInput(GLFWwindow* window)
{	
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        renderer->camera->ProcessKeyboard(FORWARD, 1/30.0f);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        renderer->camera->ProcessKeyboard(BACKWARD, 1 / 30.0f);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        renderer->camera->ProcessKeyboard(LEFT, 1 / 30.0f);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        renderer->camera->ProcessKeyboard(RIGHT, 1 / 30.0f);

    if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
        renderer->addRainParticleEmitter();

    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
        renderer->addGasParticleEmitter();

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
	{
		if (!lightingToggled)
		{
			renderer->toggleLighting();
			lightingToggled = true;
		}
	}
	else
	{
		if (lightingToggled)
			lightingToggled = false;
	}

    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
    {
        if (!gridToggled)
        {
            renderer->toggleWireframe();
            gridToggled = true;
        }
    }
    else
    {
        if (gridToggled)
            gridToggled = false;
    }

    if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS)
    {
        if (!hasTakenChangeDisplacementLayerInput)
        {
            if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS)
                renderer->addDisplacementLayer();
            else if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS)
                renderer->removeDisplacentLayer();
            hasTakenChangeDisplacementLayerInput = true;
        }
    }
    else
    {
        if (hasTakenChangeDisplacementLayerInput)
            hasTakenChangeDisplacementLayerInput = false;
    }

    if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS)
    {
        if (!hasTakenChangeSecondaryDisplacementLayerInput)
        {
            if (glfwGetKey(window, GLFW_KEY_H) == GLFW_PRESS)
                renderer->addSecondaryDisplacementLayer();
            else if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS)
                renderer->removeSecondaryDisplacementLayer();

            hasTakenChangeSecondaryDisplacementLayerInput = true;
        }
    }
    else
    {
        if (hasTakenChangeSecondaryDisplacementLayerInput)
            hasTakenChangeSecondaryDisplacementLayerInput = false;
    }

    if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
    {
        if (!hasTakenESMInput)
        {
            if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
                renderer->addESMIntensity();
            else if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
                renderer->removeESMIntensity();
            hasTakenESMInput = true;
        }
    }
    else
    {
        if (hasTakenESMInput)
            hasTakenESMInput = false;
    }

    if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
    {
        if (!hasTakenTessInput)
        {
            if (glfwGetKey(window, GLFW_KEY_4) == GLFW_PRESS)
                renderer->addTessellationLevel();
            else if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
                renderer->removeTessellationLevel();
            hasTakenTessInput = true;
        }
    }
    else
    {
        if (hasTakenTessInput)
            hasTakenTessInput = false;
    }
}

bool firstMouse = true;
float lastX = 0.0f;
float lastY = 0.0f;

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	//renderer->camera->mouseRotate(xpos, ypos);
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos;

    lastX = xpos;
    lastY = ypos;

    renderer->camera->ProcessMouseMovement(xoffset, yoffset);
}