#include "BlurredDepthMap.h"



BlurredDepthMap::BlurredDepthMap(size_t width, size_t height) : width(width), height(height)
{
    glGenFramebuffers(1, &FBO);
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    
    glGenTextures(1, &horizontalBlurredDepthmapID);
    glBindTexture(GL_TEXTURE_2D, horizontalBlurredDepthmapID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, horizontalBlurredDepthmapID, 0);
    
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    glGenTextures(1, &blurredDepthmapID);
    glBindTexture(GL_TEXTURE_2D, blurredDepthmapID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, width, height, 0, GL_RED, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    glBindTexture(GL_TEXTURE_2D, 0);

    shader = new GLShader("Shaders/DepthBlurred.vert", "Shaders/DepthBlurred.frag");
    VertexAttributes vAttrs(nullptr, 0, nullptr, 0);
    vAttrs.set_2d_coordinate_attributes();

    vbo = new VBO(vAttrs);
    vao = new VAO(*vbo, &vAttrs);
}

void BlurredDepthMap::draw(Texture* depthMap)
{
    glBindFramebuffer(GL_FRAMEBUFFER, FBO);
    
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
    glReadBuffer(GL_NONE);

    glViewport(0, 0, width, height);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, horizontalBlurredDepthmapID, 0);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    
    shader->use();
    shader->setInt("depthMap", 1);
    shader->setBool("horizontal", true);
    depthMap->bind();

    vao->bind();
    glDrawArrays(GL_TRIANGLES, 0, 3);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, blurredDepthmapID, 0);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, horizontalBlurredDepthmapID);

    shader->setBool("horizontal", false);
    glDrawArrays(GL_TRIANGLES, 0, 3);

    vao->unbind();

    depthMap->unbind();
    
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
}


BlurredDepthMap::~BlurredDepthMap()
{
    delete shader;
    delete vbo;
    delete vao;
}
