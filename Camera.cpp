#include "Camera.h"

Camera::Camera()
{
	pitch = yaw = 0;

	view = glm::mat4(1.0f);
	eyeVector = glm::vec3(0.0f, 0.0f, -1.0f);
	mousePosition = glm::vec3(0.0f, 0.0f, 0.0f);
	camera_quat = glm::quat(glm::vec3(pitch, yaw, 0));
	updateView();
}

Camera::~Camera()
{
}

void Camera::update()
{
	eyeVector += velocity;
}

void Camera::draw(const GLShader &shader)
{
	updateView();
	shader.setMatrix("view", view);
	velocity = glm::vec3(0.0f);
}

void Camera::ProcessKeyboard(GLFWwindow* window)
{
	if (!lockCamera)
	{
		float dx = 0;
		float dz = 0;

		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			dz += 2;
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			dz -= 2;
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
			dx -= 2;
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
			dx += 2;

		glm::vec3 forward(view[0][2], view[1][2], view[2][2]);
		glm::vec3 strafe(view[0][0], view[1][0], view[2][0]);

		if (dz != 0 && dx != 0)
		{
			dz /= 2;
			dx /= 2;
		}

		velocity = (-dz * forward + dx * strafe) * movementSpeed;
		dx = 0;
		dz = 0;

		updateView();
	}
}

void Camera::mouseRotate(double xpos, double ypos)
{
	if (!lockCamera)
	{
		if (!cameraMoved)
		{
			mousePosition = glm::vec2(xpos, ypos);
			cameraMoved = true;
		}

		glm::vec2 mouse_delta = glm::vec2(xpos, ypos) - mousePosition;

		yaw += mouseSensitivity * mouse_delta.x;
		pitch += mouseSensitivity * mouse_delta.y;

		mousePosition = glm::vec2(xpos, ypos);
		updateView();
	}
}

void Camera::updateView()
{
	glm::mat4 rotationMat = glm::mat4(1.0f);

	if (!lockCamera)
	{
		glm::mat4 matPitch = glm::mat4(1.0f);
		glm::mat4 matYaw = glm::mat4(1.0f);

		matPitch = glm::rotate(matPitch, pitch, glm::vec3(1.0f, 0.0f, 0.0f));
		matYaw = glm::rotate(matYaw, yaw, glm::vec3(0.0f, 1.0f, 0.0f));

		rotationMat = matPitch * matYaw;
	}
	else if (rotationSet)
	{
		rotationMat = glm::mat4_cast(rotation);
	}

	glm::mat4 translate = glm::mat4(1.0f);
	translate = glm::translate(translate, -eyeVector);

	view = rotationMat * translate;
}

void Camera::setLookAtVals(glm::vec3& cameraPos, glm::vec3& lookAt, float& tMax, const glm::mat4& proj)
{
    glm::mat4 matPitch = glm::mat4(1.0f);
    glm::mat4 matYaw = glm::mat4(1.0f);

    matPitch = glm::rotate(matPitch, pitch, glm::vec3(1.0f, 0.0f, 0.0f));
    matYaw = glm::rotate(matYaw, yaw, glm::vec3(0.0f, 1.0f, 0.0f));

    lookAt = -(glm::vec3(0, 0, 1) * glm::quat(matPitch * matYaw));
    cameraPos = getPos();
    tMax = 100;
}
