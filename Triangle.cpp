#include "Triangle.h"

Triangle::Triangle(glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
    points[0] = a;
    points[1] = b;
    points[2] = c;
    setup();
    init();
}

Triangle::~Triangle()
{

}

void Triangle::setup()
{
    createVertices();
    vAttrs = new VertexAttributes(vertices, 9 * sizeof(float), indices, sizeof(indices));
    vAttrs->set_coordinate_attributes();
}

void Triangle::createVertices()
{
    vertices[0] = points[0].x;
    vertices[1] = points[0].y;
    vertices[2] = points[0].z;

    vertices[3] = points[1].x;
    vertices[4] = points[1].y;
    vertices[5] = points[1].z;

    vertices[6] = points[2].x;
    vertices[7] = points[2].y;
    vertices[8] = points[2].z;
}

void Triangle::draw(const GLShader& shader) const
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    vao->bind();
    shader.setMatrix("model", model);
    glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, indices);
    vao->unbind();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

bool Triangle::intersects(const glm::vec3& origin, const glm::vec3& rayVec, glm::vec3& intersectionPoint) const
{
    return glm::intersectLineTriangle(origin, rayVec, points[0], points[1], points[2], intersectionPoint);
}
