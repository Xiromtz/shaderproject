#include "MC_Geometry.h"



MC_Geometry::MC_Geometry(glm::vec3* vertices, size_t size)
{
    setVertexPositions(vertices, size);
    //kdTree = new KD_Tree(vertexPositions);

    scale(glm::vec3(5.0f, 10.0f, 5.0f));
    update();

    setTriangles();

    tex = new Texture("Texture/lichen1.tga", GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR, GL_TEXTURE0, GL_RGBA, GL_UNSIGNED_BYTE);
    tex2 = new Texture("Texture/lichen2.tga", GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR, GL_TEXTURE1, GL_RGBA, GL_UNSIGNED_BYTE);
    tex3 = new Texture("Texture/lichen3.tga", GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR, GL_TEXTURE2, GL_RGBA, GL_UNSIGNED_BYTE);

    vertexCount = size;
    this->vertices = vertices;
    vAttrs = new VertexAttributes(reinterpret_cast<float*>(vertices), sizeof(GLfloat) * size * 3, nullptr, 0);
    vAttrs->set_coordinate_color_attributes();
    init();

}

void MC_Geometry::setVertexPositions(glm::vec3* vertices, size_t size)
{
    vertexPositions.resize(size / 2);
    int u = 0;

    for (size_t i = 0u; i < size; i += 6)
    {
        vertexPositions[u] = vertices[i];
        vertexPositions[u + 1] = vertices[i + 2];
        vertexPositions[u + 2] = vertices[i + 4];
        u += 3;
    }
}

void MC_Geometry::setTriangles()
{
    for (size_t i = 0u; i < vertexPositions.size(); i+=3)
    {
        glm::vec3 p1 = model * glm::vec4(vertexPositions[i], 1.0f);
        glm::vec3 p2 = model * glm::vec4(vertexPositions[i + 1], 1.0f);
        glm::vec3 p3 = model * glm::vec4(vertexPositions[i + 2], 1.0f);

        Triangle* triangle = new Triangle(p1, p2, p3);
        triangles.push_back(triangle);
    }
}


void MC_Geometry::draw(const GLShader& shader) const
{
    tex->bind();
    tex2->bind();
    tex3->bind();
    vao->bind();
    
    shader.setMatrix("model", model);

    glPatchParameteri(GL_PATCH_VERTICES, 3);
    glDrawArrays(GL_PATCHES, 0, vertexCount);

    vao->unbind();
    tex->unbind();
    tex2->unbind();
    tex3->unbind();
}

Triangle* MC_Geometry::getClosestIntersectingTriangle(const glm::vec3& origin, const glm::vec3& d)
{
    Triangle* closestTriangle = nullptr;
    float closestDistance = FLT_MAX;

    for (Triangle* triangle : triangles)
    {
        glm::vec3 intersectionPoint;
        if (triangle->intersects(origin, d, intersectionPoint))
        {
            float dist = glm::distance(intersectionPoint, origin);
            if (dist < closestDistance)
            {
                closestDistance = dist;
                closestTriangle = triangle;
            }
        }
    }

    return closestTriangle;
}

std::vector<Triangle*> MC_Geometry::getTriangles() const
{
    return triangles;
}


MC_Geometry::~MC_Geometry()
{
    delete tex2;
    delete tex3;
    //delete kdTree;
    for (size_t i = 0u; i < triangles.size(); i++)
    {
        delete triangles[i];
    }
}
