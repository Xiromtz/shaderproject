#include "DepthMap.h"
#include "Renderer.h"

DepthMap::DepthMap()
{
	depthMap = new Texture(SHADOW_WIDTH, SHADOW_HEIGHT, GL_CLAMP_TO_BORDER, GL_CLAMP_TO_BORDER, GL_NEAREST, GL_NEAREST, GL_TEXTURE1, GL_R32F, GL_RED, GL_FLOAT);

	setupBuffer();
	setupShader();
}

void DepthMap::setupBuffer()
{
	glGenFramebuffers(1, &ID);
	glBindFramebuffer(GL_FRAMEBUFFER, ID);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, depthMap->ID, 0);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DepthMap::setupShader()
{
	shader = new GLShader("Shaders/Depth.vert", "Shaders/Depth.frag");
}

DepthMap::~DepthMap()
{
	delete depthMap;
	delete shader;
}

void DepthMap::bind() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, ID);

	glCullFace(GL_FRONT);
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	shader->use();
	
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
}

void DepthMap::unbind() const
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glCullFace(GL_BACK);
}