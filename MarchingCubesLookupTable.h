#pragma once
#include <glad/glad.h>
#include "GLShader.h"

struct CellTriangles
{
    int tris[16];
};

struct MC_TrisTable
{
    CellTriangles triTable[256];
};

struct MC_EdgeTable
{
    GLint edges[256];
};

class MarchingCubesLookupTable
{
private:
    static const MC_TrisTable m_trisTable;
    static const MC_EdgeTable m_edgeTable;

    GLuint m_trisIndex;
    GLuint m_edgeTableIndex;

    void setupUniformBuffers();
public:
    

    MarchingCubesLookupTable();
    ~MarchingCubesLookupTable();

    void BindTablesToShader(const GLShader& shader);
};

