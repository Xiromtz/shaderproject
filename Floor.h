#pragma once
#include "GLObject.h"
#include "Triangle.h"

class Floor : public GLObject
{
private:
    Texture * normalTex;
    std::vector<Triangle*> triangles;
    const char* texturePath = "Texture/floor.jpg";
    const char* normalTexturePath = "Texture/floor_normal.jpg";

    float* vertices = new float[32]
    {
        // positions			// normals			// texcoords
        -20.0f, 0.0f, -20.0f,	0.0f, 1.0f, 0.0f,	0.0f, 0.0f,
        20.0f, 0.0f, -20.0f,	0.0f, 1.0f, 0.0f,	10.0f, 0.0f,
        20.0f, 0.0f, 20.0f,		0.0f, 1.0f, 0.0f,	10.0f, 10.0f,
        -20.0f, 0.0f, 20.0f,	0.0f, 1.0f, 0.0f,	0.0f, 10.0f
    };

    unsigned int indices[6]
    {
        0,1,2,
        2,3,0
    };

    void setup();
    void addTangents();

public:
    explicit Floor();
    explicit Floor(const char* texturePath, const char* normalTexturePath);
    ~Floor();

    void draw(const GLShader &shader) const override;
    void setTriangles();
    std::vector<Triangle*> getTriangles() const override;
    void update() override;
};

