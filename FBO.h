#pragma once
#include <glad/glad.h>
#include <stdexcept>
#include "Texture.h"

class FBO
{
public:
    GLuint ID;

    FBO();
    ~FBO();

    void bindTexture(const Texture& texture);
    void bind();
    void unbind();
};

