#pragma once
#include <glad/glad.h>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>

class GLShader
{
private:
	const char* defaultVertexShaderPath = "Shaders/VertexShader.vert";
	const char* defaultFragmentShaderPath = "Shaders/FragmentShader.frag";
	
    GLuint setupShaderProgramForTransformFeedback(const GLuint vertexShader, const GLuint geometryShader, const std::vector<GLchar*>& feedbackVaryings);
	GLuint setupShaderProgram(const GLuint vertexShader, const GLuint fragmentShader);
    GLuint setupShaderProgram(const GLuint vertexShader, const GLuint geometryShader, const GLuint fragmentShader);
	GLuint setupShaderProgram(const GLuint vertexShader, const GLuint tessControllerShader, const GLuint tessEvalShader, const GLuint geometryShader, const GLuint fragmentShader);
	GLuint LoadVertexShader(const char *path) const;
	GLuint LoadTessControllerShader(const char* path) const;
	GLuint LoadTessEvalShader(const char* path) const;
	GLuint LoadFragmentShader(const char *path) const;
    GLuint LoadGeometryShader(const char* path) const;
	GLuint LoadShader(const char* path, const GLenum type) const;

	std::string readFile(const char* path) const;
public:
	
	GLuint ID;
	
    GLShader(const char* vertexPath, const char* geometryPath, const std::vector<GLchar*>& feedbackVaryings);
	GLShader(const char* vertexPath, const char* fragmentPath);
    GLShader(const char* vertexPath, const char* geometryPath, const char* fragmentPath);
	GLShader(const char* vertexPath, const char* tessControllerPath, const char* tessEvalPath, const char* geometryPath, const char* fragmentPath);
	~GLShader();
	
	inline void use() const
	{
		glUseProgram(ID);
	}

	void setBool(const std::string &name, bool value) const;
	void setInt(const std::string &name, int value) const;
	void setFloat(const std::string &name, float value) const;
	void setMatrix(const std::string &name, glm::mat4 value) const;
	void setVec3(const std::string &name, glm::vec3 value) const;
    void setVec2(const std::string &name, glm::vec2 value) const;
    void setVec4(const std::string &name, glm::vec4 value) const;
};

