#pragma once
#include "VertexAttributes.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include "VAO.h"
#include "EBO.h"
#include "GLShader.h"
#include "Texture.h"

class GLObject
{
protected:
	glm::mat4 model;
	glm::mat4 rotationMat;
	glm::mat4 scaleMat;
	glm::mat4 translationMat;

	VBO* vbo = nullptr;
	VAO* vao = nullptr;

	VertexAttributes* vAttrs = nullptr;
	Texture* tex = nullptr;

public:
	explicit GLObject()
	{
        model = glm::mat4(1.0f);

		rotationMat = glm::mat4(1.0f);
		scaleMat = glm::mat4(1.0f);
		translationMat = glm::mat4(1.0f);
	}
	virtual ~GLObject()
	{
		delete vbo;
		delete vao;

		delete vAttrs;
		delete tex;
	}

	void init()
	{
        vbo = new VBO(*vAttrs);
        vao = new VAO(*vbo, vAttrs);
	}

	virtual void draw(const GLShader &shader) const = 0;
    virtual std::vector<class Triangle*> getTriangles() const = 0;
	virtual void update()
	{
		updateModel();
	}

	inline void updateModel()
	{
		model = translationMat * rotationMat * scaleMat;
	}

	inline void translate(glm::vec3 move)
	{
		translationMat = glm::translate(translationMat, move);
	}

	inline void rotate(float degrees, glm::vec3 axis)
	{
		rotationMat = glm::rotate(rotationMat, glm::radians(degrees), axis);
	}

	inline void rotate(glm::quat rotation)
	{
		rotationMat = rotationMat * glm::mat4_cast(rotation);
	}

	inline void scale(glm::vec3 size)
	{
		scaleMat = glm::scale(scaleMat, size);
	}

	inline void resetMatrix()
	{
		model = glm::mat4(1.0f);
		translationMat = glm::mat4(1.0f);
		rotationMat = glm::mat4(1.0f);
		scaleMat = glm::mat4(1.0f);
	}
};

