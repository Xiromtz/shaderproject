#include "GLShader.h"


GLShader::GLShader(const char* vertexPath, const char* fragmentPath)
{
	GLuint vertexShader = LoadVertexShader(vertexPath);
	if (vertexShader == 0)
		return;

	GLuint fragmentShader = LoadFragmentShader(fragmentPath);
	if (fragmentShader == 0)
		return;

	ID = setupShaderProgram(vertexShader, fragmentShader);
	if (ID == 0)
		return;

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

GLShader::GLShader(const char* vertexPath, const char* geometryPath, const char* fragmentPath)
{
    GLuint vertexShader = LoadVertexShader(vertexPath);
    if (vertexShader == 0)
        return;

    GLuint geometryShader = LoadGeometryShader(geometryPath);
    if (geometryShader == 0)
        return;

    GLuint fragmentShader = LoadFragmentShader(fragmentPath);
    if (fragmentShader == 0)
        return;

    ID = setupShaderProgram(vertexShader, geometryShader, fragmentShader);
    if (ID == 0)
        return;

    glDeleteShader(vertexShader);
    glDeleteShader(geometryShader);
    glDeleteShader(fragmentShader);
}

GLShader::GLShader(const char* vertexPath, const char* geometryPath, const std::vector<GLchar*>& feedbackVaryings)
{
    GLuint vertexShader = LoadVertexShader(vertexPath);
    if (vertexShader == 0)
        return;

    GLuint geometryShader = LoadGeometryShader(geometryPath);
    if (geometryShader == 0)
        return;

    ID = setupShaderProgramForTransformFeedback(vertexShader, geometryShader, feedbackVaryings);
    if (ID == 0)
        return;

    glDeleteShader(vertexShader);
    glDeleteShader(geometryShader);
}

GLShader::GLShader(const char* vertexPath, const char* tessControllerPath, const char* tessEvalPath, const char* geometryPath, const char* fragmentPath)
{
	GLuint vertexShader = LoadVertexShader(vertexPath);
	if (vertexShader == 0)
		return;

	GLuint tessControllerShader = LoadTessControllerShader(tessControllerPath);
	if (tessControllerShader == 0)
		return;

	GLuint tessEvalShader = LoadTessEvalShader(tessEvalPath);
	if (tessEvalShader == 0)
		return;

	/*GLuint geometryShader = LoadGeometryShader(geometryPath);
	if (geometryShader == 0)
		return;*/

	GLuint fragmentShader = LoadFragmentShader(fragmentPath);
	if (fragmentShader == 0)
		return;

	ID = setupShaderProgram(vertexShader, tessControllerShader, tessEvalShader, 0, fragmentShader);
	if (ID == 0)
		return;

	glDeleteShader(vertexShader);
	glDeleteShader(tessControllerShader);
	glDeleteShader(tessEvalShader);
	glDeleteShader(fragmentShader);
}

unsigned GLShader::setupShaderProgramForTransformFeedback(const unsigned vertexShader, const unsigned geometryShader, const std::vector<GLchar*>& feedbackVaryings)
{
    const GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, geometryShader);

    glTransformFeedbackVaryings(shaderProgram, feedbackVaryings.size(), feedbackVaryings.data(), GL_INTERLEAVED_ATTRIBS);

    glLinkProgram(shaderProgram);

    int success;
    char infolog[512];
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(shaderProgram, 512, nullptr, infolog);
        std::cout << "ERROR::SHADER::PROGRAM::COMPILATION::FAILED\n" << infolog << std::endl;
        return NULL;
    }

    return shaderProgram;
}


GLuint GLShader::setupShaderProgram(const GLuint vertexShader, const GLuint fragmentShader)
{
	const GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	int success;
	char infolog[512];
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram, 512, nullptr, infolog);
		std::cout << "ERROR::SHADER::PROGRAM::COMPILATION::FAILED\n" << infolog << std::endl;
		return NULL;
	}

	return shaderProgram;
}

GLuint GLShader::setupShaderProgram(const GLuint vertexShader, const GLuint geometryShader, const GLuint fragmentShader)
{
    const GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, geometryShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    int success;
    char infolog[512];
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(shaderProgram, 512, nullptr, infolog);
        std::cout << "ERROR::SHADER::PROGRAM::COMPILATION::FAILED\n" << infolog << std::endl;
        return NULL;
    }

    return shaderProgram;
}

GLuint GLShader::setupShaderProgram(const GLuint vertexShader, const GLuint tessControllerShader, const GLuint tessEvalShader, const GLuint geometryShader, const GLuint fragmentShader)
{
	const GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, tessControllerShader);
	glAttachShader(shaderProgram, tessEvalShader);
	//glAttachShader(shaderProgram, geometryShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	int success;
	char infolog[512];
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram, 512, nullptr, infolog);
		std::cout << "ERROR::SHADER::PROGRAM::COMPILATION::FAILED\n" << infolog << std::endl;
		return NULL;
	}

	return shaderProgram;
}


GLuint GLShader::LoadVertexShader(const char* path) const
{
	return LoadShader(path, GL_VERTEX_SHADER);
}

GLuint GLShader::LoadTessControllerShader(const char* path) const
{
	return LoadShader(path, GL_TESS_CONTROL_SHADER);
}

GLuint GLShader::LoadTessEvalShader(const char* path) const
{
	return LoadShader(path, GL_TESS_EVALUATION_SHADER);
}

GLuint GLShader::LoadFragmentShader(const char* path) const
{
	return LoadShader(path, GL_FRAGMENT_SHADER);
}

unsigned GLShader::LoadGeometryShader(const char* path) const
{
    return LoadShader(path, GL_GEOMETRY_SHADER);
}


GLuint GLShader::LoadShader(const char* path, const GLenum type) const
{
	const GLuint shader = glCreateShader(type);
	std::string shaderString = readFile(path);

	if (shaderString.size() <= 0)
		return NULL;

	const char* shader_cString = shaderString.c_str();
	glShaderSource(shader, 1, &(shader_cString), nullptr);
	glCompileShader(shader);

	int success;
	char infoLog[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(shader, 512, nullptr, infoLog);
		std::cout << "ERROR:SHADER::VERTEX::COMPILATION::FAILED\n" << infoLog << std::endl;
		return NULL;
	}
	return shader;
}


std::string GLShader::readFile(const char* path) const
{
	std::string code;
	std::ifstream file;

	file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		file.open(path);
		std::stringstream shaderStream;
		shaderStream << file.rdbuf();
		file.close();
		code = shaderStream.str();
	}
	catch (std::ifstream::failure e)
	{
		std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY READ: " << e.what() << std::endl;
	}

	return code;
}

void GLShader::setBool(const std::string& name, bool value) const
{
	glUniform1i(glGetUniformLocation(ID, name.c_str()), static_cast<int>(value));
}

void GLShader::setInt(const std::string& name, int value) const
{
	glUniform1iv(glGetUniformLocation(ID, name.c_str()), 1, &value);
}

void GLShader::setFloat(const std::string& name, float value) const
{
	glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}

void GLShader::setMatrix(const std::string& name, glm::mat4 value) const
{
	glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, glm::value_ptr(value));
}

void GLShader::setVec2(const std::string& name, glm::vec2 value) const
{
    glUniform2fv(glGetUniformLocation(ID, name.c_str()), 1, glm::value_ptr(value));
}

void GLShader::setVec3(const std::string& name, glm::vec3 value) const
{
	glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, glm::value_ptr(value));
}

void GLShader::setVec4(const std::string& name, glm::vec4 value) const
{
    glUniform4fv(glGetUniformLocation(ID, name.c_str()), 1, glm::value_ptr(value));
}

GLShader::~GLShader()
{
}
