#include "Cube.h"

Cube::Cube()
{
    setup();
}

Cube::Cube(const char* texturePath, const char* normalTexturePath) : texturePath(texturePath), normalTexturePath(normalTexturePath)
{
    setup();
}

void Cube::setup()
{
    addTangents();
    vAttrs = new VertexAttributes(vertices, 264 * sizeof(float), indices, sizeof(this->indices));
    vAttrs->set_coordinate_normal_texture_tangent_attributes();
    tex = new Texture(texturePath, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR, GL_TEXTURE0, GL_RGB, GL_UNSIGNED_BYTE);
    normalTex = new Texture(normalTexturePath, GL_REPEAT, GL_REPEAT, GL_LINEAR, GL_LINEAR, GL_TEXTURE2, GL_RGB, GL_UNSIGNED_BYTE);
    init();
}

void Cube::addTangents()
{
    int tempIndex = 0;
    float* tempVertices = new float[264];
    for (int i = 0; i < 6; i++)
    {
        int vertexStartIndex = i * 32;
        int tempStartIndex = i * 44;

        glm::vec3 pos1(vertices[vertexStartIndex], vertices[vertexStartIndex + 1], vertices[vertexStartIndex + 2]);
        glm::vec2 uv1(vertices[vertexStartIndex + 6], vertices[vertexStartIndex + 7]);
        glm::vec3 pos2(vertices[vertexStartIndex + 8], vertices[vertexStartIndex + 9], vertices[vertexStartIndex + 10]);
        glm::vec2 uv2(vertices[vertexStartIndex + 14], vertices[vertexStartIndex + 15]);
        glm::vec3 pos3(vertices[vertexStartIndex + 16], vertices[vertexStartIndex + 17], vertices[vertexStartIndex + 18]);
        glm::vec2 uv3(vertices[vertexStartIndex + 22], vertices[vertexStartIndex + 23]);

        glm::vec3 edge1 = pos2 - pos1;
        glm::vec3 edge2 = pos3 - pos1;
        glm::vec2 deltaUV1 = uv2 - uv1;
        glm::vec2 deltaUV2 = uv3 - uv1;

        glm::vec3 tangent;

        float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);
        tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
        tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
        tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);
        tangent = glm::normalize(tangent);

        for (int u = 0; u < 8; u++)
        {
            tempVertices[tempStartIndex + u] = vertices[vertexStartIndex + u];
        }

        tempVertices[tempStartIndex + 8] = tangent.x;
        tempVertices[tempStartIndex + 9] = tangent.y;
        tempVertices[tempStartIndex + 10] = tangent.z;


        for (int u = 8; u < 16; u++)
        {
            tempVertices[tempStartIndex + u + 3] = vertices[vertexStartIndex + u];
        }

        tempVertices[tempStartIndex + 19] = tangent.x;
        tempVertices[tempStartIndex + 20] = tangent.y;
        tempVertices[tempStartIndex + 21] = tangent.z;

        for (int u = 16; u < 24; u++)
        {
            tempVertices[tempStartIndex + u + 6] = vertices[vertexStartIndex + u];
        }

        tempVertices[tempStartIndex + 30] = tangent.x;
        tempVertices[tempStartIndex + 31] = tangent.y;
        tempVertices[tempStartIndex + 32] = tangent.z;

        for (int u = 24; u < 32; u++)
        {
            tempVertices[tempStartIndex + u + 9] = vertices[vertexStartIndex + u];
        }

        tempVertices[tempStartIndex + 41] = tangent.x;
        tempVertices[tempStartIndex + 42] = tangent.y;
        tempVertices[tempStartIndex + 43] = tangent.z;
    }

    delete[] vertices;
    vertices = tempVertices;
}


Cube::~Cube()
{
    delete normalTex;
    delete[] vertices;
    for (auto it = triangles.begin(); it != triangles.end(); ++it)
    {
        delete *it;
    }
}

void Cube::draw(const GLShader& shader) const
{
    tex->bind();
    normalTex->bind();
    vao->bind();
    shader.setMatrix("model", model);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, indices);
    vao->unbind();
    normalTex->unbind();
    tex->unbind();
}

void Cube::setTriangles()
{
    int vertexWidth = 11;
    for (int i = 0; i < 36;)
    {
        int currentStartPos = indices[i] * vertexWidth;
        glm::vec3 vertex1(vertices[currentStartPos], vertices[currentStartPos + 1], vertices[currentStartPos + 2]);
        vertex1 = model * glm::vec4(vertex1, 1.0f);
        i++;
        currentStartPos = indices[i] * vertexWidth;
        glm::vec3 vertex2(vertices[currentStartPos], vertices[currentStartPos + 1], vertices[currentStartPos + 2]);
        vertex2 = model * glm::vec4(vertex2, 1.0f);
        i++;
        currentStartPos = indices[i] * vertexWidth;
        glm::vec3 vertex3(vertices[currentStartPos], vertices[currentStartPos + 1], vertices[currentStartPos + 2]);
        vertex3 = model * glm::vec4(vertex3, 1.0f);
        i++;

        Triangle* triangle = new Triangle(vertex1, vertex2, vertex3);
        triangles.push_back(triangle);
    }
}

std::vector<Triangle*> Cube::getTriangles() const
{
    return triangles;
}

Triangle* Cube::getIntersectingTriangle(const glm::vec3& origin, const glm::vec3& rayDirection, float tMax)
{
    Triangle* closestTriangle = nullptr;
    float closestDistance = FLT_MAX;

    for (Triangle* triangle : triangles)
    {
        glm::vec3 intersectionPoint;
        if (triangle->intersects(origin, rayDirection, intersectionPoint))
        {
            float dist = glm::distance(intersectionPoint, origin);
            if (dist < closestDistance)
            {
                closestDistance = dist;
                closestTriangle = triangle;
            }
        }
    }
    return closestTriangle;
}
